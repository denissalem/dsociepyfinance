#! /usr/bin/env python3

import datetime

def print_history(history, prompt=''):
    good_trades = [v.trade_rate for v in history if v.trade_rate > 0]
    bad_trades = [v.trade_rate for v in history if v.trade_rate <= 0]
    good_trades_ratio = len(good_trades) / len(history)
    bad_trades_ratio = len(bad_trades) / len(history)
    good_trade_rate_avr = sum(good_trades) / len(good_trades) if len(good_trades) else 0
    bad_trade_rate_avr = sum(bad_trades) / len(bad_trades) if len(bad_trades) else 0
    
    print(
      prompt,
      datetime.datetime.fromtimestamp(history[-1].timestamp),
      "\033[92mBuy\033[0m" if history[-1].action == "green" else "\033[91mSell\033[0m",
      "PORTEFOLIO:", "{:.2f}".format(history[-1].portefolio_value), 
      "TRADE RATE:", ( ' \033[92m' if history[-1].trade_rate > 0 else '\033[91m') + "{:.2f}".format(history[-1].trade_rate) + '\033[0m',
      
      "+TRD RATIO:", ( '\033[92m' if good_trades_ratio > bad_trades_ratio else '\033[91m') + ("{:.2f}".format(good_trades_ratio)) + '\033[0m',
      "-TRD RATIO:", ( '\033[92m' if good_trades_ratio > bad_trades_ratio else '\033[91m') + ("{:.2f}".format(bad_trades_ratio))  + '\033[0m',
      
      "+TRD AVR:",   ( '\033[92m' if good_trade_rate_avr > abs(bad_trade_rate_avr) else '\033[91m') + ("{:.2f}".format(good_trade_rate_avr)) + '\033[0m',
      "-TRD AVR:",   ( '\033[92m' if good_trade_rate_avr > abs(bad_trade_rate_avr) else '\033[91m') + ("{:.2f}".format(bad_trade_rate_avr)) + '\033[0m',
      "IS STOP LOSS" if history[-1].stop_loss else ""
    )
  
def get_performances_informations(history):
    good_trades = [v.trade_rate for v in history if v.trade_rate > 0]
    bad_trades = [v.trade_rate for v in history if v.trade_rate <= 0]
    good_trades_ratio = len(good_trades) / len(history)
    bad_trades_ratio = len(bad_trades) / len(history)
    good_trade_rate_avr = sum(good_trades) / len(good_trades) if len(good_trades) else 0
    bad_trade_rate_avr = sum(bad_trades) / len(bad_trades) if len(bad_trades) else 0
    
    return {
        "good_trade": good_trades,
        "bad_trade": bad_trade,
        "good_trades_ratio" : good_trades_ratio,
        "bad_trades_ratio" : bad_trades_ratio,
        "good_trade_rate_avr" : good_trade_rate_avr,
        "bad_trade_rate_avr" : bad_trade_rate_avr
    }
