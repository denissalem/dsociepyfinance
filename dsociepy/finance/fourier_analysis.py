#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np

def power_spectrum_from_dft(signal):
    samples_count = len(signal)
    dft = np.fft.fft(signal, samples_count)
    power_spectrum = dft * np.conj(dft) / samples_count
    return power_spectrum[0:len(power_spectrum)//2]
