#!/usr/bin/env python3

#    Copyright 2019, 2024 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.pyplot import savefig

def stacked_time_frequency(x, y, coefs, freqs, save_fig=None):
    levels = [0.0625, 0.125, 0.25, 0.5, 1, 2, 4, 8]

    fig = plt.figure(figsize=(16, 9))
    gs = gridspec.GridSpec(nrows=2, ncols=1, )
    
    ax1 = fig.add_subplot(gs[:1, 0])
    ax2 = fig.add_subplot(gs[1:, 0])
    
    ax1.get_xaxis().set_ticks([])
    ax1.get_yaxis().set_ticks([])
    ax2.get_xaxis().set_ticks([])
    ax2.get_yaxis().set_ticks([])  

    ax1.set_xlim(x[0], x[-1]);
    ax1.plot(x, y, linewidth=0.5)

    ax2.invert_yaxis()
    ax2.contourf(
        x,
        np.log2(1./freqs),
        np.log2(abs(coefs)**2),
        np.log2(levels),
        extend='both'
    )

    gs.tight_layout(fig)
    plt.subplots_adjust(wspace=1, hspace=1)
    if save_fig != None:
        savefig(
            save_fig,
            transparent=False,
            dpi='figure',
            format="png",
            metadata=None,
            bbox_inches=None,
            pad_inches=0.1,
            facecolor='auto',
            edgecolor='auto',
            backend=None
        )
    else:
      plt.show()
