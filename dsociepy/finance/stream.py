#! /usr/bin/env python3

import os
import json

class Context:
    def __init__(self, data_streamer, filename_index):
        self.data_streamer = data_streamer
        self.filename_index = filename_index
        self.reload()
        
    def reload(self):
        self.trade_index = 0
        self.data = sorted(
            json.loads(
                open(
                    self.data_streamer.source+"/"+self.data_streamer.filenames[self.filename_index], 
                    "r"
                ).read()
            ),
            key=lambda x:x[2]
        )
        
    def rewind(self):
        if self.trade_index > 0:
            self.trade_index -= 1
            
        else:
            self.filename_index-=1
            self.reload()
            self.trade_index = len(self.data)-1
        
                
class LocalDataStreamer:
    def __init__(self, source, begin_from_filename=None):
        self.source = os.path.expanduser(source)
        self.filenames = sorted(os.listdir(self.source))

        self.trades_position = Context(
            self, 
            self._filenames.index(begin_from_filename) if begin_from_filename != None else 0
        )
        
    def stream_trades(self):
        output = self.trades_position.data[self.trades_position.trade_index]
        if self.trades_position.trade_index + 1 < len(self.trades_position.data):
            self.trades_position.trade_index += 1
        else:
            self.trades_position.filename_index +=1
            self.trades_position.reload()
            
        return output
        


