#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.


import io
import json
import pycurl
import time
import urllib.parse

# ~ from dsociepy.finance.market_interfaces.exceptions import ResponseIsNotJSON
# ~ from dsociepy.finance.market_interfaces.exceptions import APINotAvailable
# ~ from dsociepy.finance.market_interfaces.exceptions import APIErrors

class Kraken:
    def public_query(post_fields, endpoint):
        buffer = io.BytesIO()
        query = pycurl.Curl()
        query.setopt(query.URL, "https://api.kraken.com/0/public/"+endpoint)
        query.setopt(query.WRITEFUNCTION, buffer.write)
        if post_fields != None:
            query.setopt(query.POSTFIELDS, post_fields)
        query.perform()
        response = buffer.getvalue().decode('UTF-8')
        buffer.close()
        return json.loads(response)

    def OHLC(pair, since, interval):
        post_fields = urllib.parse.urlencode({
            "pair":pair,
            "since":since,
            "interval":interval
        })
        return Kraken.public_query(post_fields, "OHLC")

    def recent_trades(pair, since):
        post_fields = urllib.parse.urlencode({
            "pair":pair,
            "since":since,
        })
        return Kraken.public_query(post_fields, "Trades")
        
    def recent_spreads(pair):
        post_fields = None
        return Kraken.public_query(post_fields, "Spread?pair="+pair)
