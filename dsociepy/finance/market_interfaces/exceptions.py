#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import datetime

class ResponseIsNotJSON(Exception):
    def __init__(self, response):
        super(Exception, self).__init__(
            "Response is not JSON",
            datetime.datetime.now(),
            response
        )

class APINotAvailable(Exception):
    def __init__(self, exception=None):
        super(Exception, self).__init__(
            "API not available.",
            datetime.datetime.now(),
            exception
        )

class APIErrors(Exception):
    def __init__(self, errors):
        super(Exception, self).__init__(
            '\n'.join(errors),
            datetime.datetime.now(),
            errors
        )

class EndOfSimulation(Exception):
    def __init__(self):
        super(Exception, self).__init__(
            "End of simulation.",
            datetime.datetime.now()
        )
