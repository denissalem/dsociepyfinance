#! /usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

import matplotlib

from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
    
# ~ import matplotlib.pyplot as plt
from matplotlib.figure import Figure

class FrontEnd:
    def __init__(self, update_callback, plot_height):
        self.update_callback = update_callback
        self.plot_height = plot_height
        
        self.scrollable = Gtk.ScrolledWindow()
        self.scrollable.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        
        self.scrollable.set_min_content_height(480)
        
        self.window = Gtk.Window()
        self.window.set_default_size(1024,768)
        self.window.connect("destroy", Gtk.main_quit)
        self.stack = Gtk.VBox()
        self.window.add(self.scrollable)
        self.scrollable.add(self.stack)

        self.menu = Gtk.HBox()        
        self.stack.pack_start(self.menu, expand=False, fill=False, padding=5)
        
        self.button_run = Gtk.Button(label='Run')
        self.button_pause = Gtk.Button(label='Pause')

        self.menu.pack_start(self.button_run,expand=False, fill=False, padding=5)
        self.menu.pack_start(self.button_pause,expand=False, fill=False, padding=5)
        
        self.wrappers = []
        
    def push_fig(self, fig):
        self.wrappers.append(FigureCanvas(fig))
        self.wrappers[-1].set_size_request(-1, self.plot_height)
        self.stack.pack_start(self.wrappers[-1], expand=False, fill=False, padding=5)
        
    def run(self):
        self.window.show_all()
        Gtk.main()

frontend = FrontEnd(None)

fig = Figure(figsize=(5, 3))
fig.tight_layout(pad=0)
ax = fig.add_subplot(111)
ax.plot([0, 1, 2, 3], [0, 1, 2, 3], label="Figure")
ax.set_xticks([])
ax.set_xticklabels([])
ax.set_xlabel("")
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_ylabel("")
ax.set_position([0, 0, 1, 1])
ax.legend()
ax.set_frame_on(False)
ax.margins(0)
frontend.push_fig(fig)

frontend.run()

