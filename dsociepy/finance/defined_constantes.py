#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

TIMEFRAMES = {
    60:     "%Y-%m-%d", #1m
    180:    "%Y-%m-%d", #3m
    300:    "%Y-%m-%d", #5m
    900:    "%y-%m",    #15m
    1800:   "%y-%m",    #30m
    3600:   "%y-%m",    #1h
    14400:  "%y",       #4h
    86400:  "%y",       #1D
    604800: "%y"        #1W
}
