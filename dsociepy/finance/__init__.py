#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import json
import time
import datetime

from dsociepy.finance.ohlcv import synth_ohlcv

def get_time_key(timestamp, timeframe):
    return (timestamp // timeframe) * timeframe

class Position:
    def __init__(self, context, price, volume, trade_type, stop_loss, identifier, valid_for_n_minutes, notify): #valid_for 100 years
        self.__context = context
        self.__identifier = identifier
        self.__trade_type = trade_type
        self.__volume = volume
        self.__price = price
        self.__stop_loss = stop_loss
        self.__expiration_time = time.time() + valid_for_n_minutes*60
        self.__status = "OPEN"
        self.__messages = []
        if notify:
            if trade_type == 'b':
                print('\033['+str(COLOR_YELLOW)+'m'+'>',"NEW BUY ORDER @", price,self.__context.get_money_symbol(), "WITH",volume,self.__context.get_asset_symbol(), "(~"+str(round(volume*price,4)),self.__context.get_money_symbol()+")", '\033['+str(COLOR_END)+'m')
                print()
                
            elif trade_type == 's':
                print('\033['+str(COLOR_YELLOW)+'m'+'>',"NEW SELL ORDER @", price, self.__context.get_money_symbol(), "WITH",volume,self.__context.get_asset_symbol(), "(~"+str(round(volume*price,4)),self.__context.get_money_symbol()+")",'\033['+str(COLOR_END)+'m')
                print()
        
    def get_identifier(self):
        return self.__identifier
        
    def get_trade_type(self):
        return self.__trade_type
        
    def get_price(self):
        return self.__price
        
    def get_volume(self):
        return self.__volume
        
    def get_stop_loss(self):
        return self.__get_stop_loss

    def get_portefolio_value(self, key):
        return self.__portefolio[key]

    def get_status(self):
        return self.__status
        
    def get_messages(self):
        return list(messages)
        
    def is_expired(self, t):
        if t > self.__expiration_time:
            self.__status = "CANCELLED" 
            self.__messages.append("Position cancelled due to expiration.")
        
        return t > self.__expiration_time
        
    def append_message(self, message):
        self.__messages.append(message)
        
    def update_position(self, volume_taken):
        if self.__status == "CLOSED":
            raise ValueError("Position already closed.")
        
        v = int(self.__volume)
        v -= volume_taken
        if v < 0:
            volume_taken = self.__volume
                
        self.__volume -= volume_taken
        
        if self.__volume > 0:
            self.__status = "PARTIAL"
        else:
            self.__status = "CLOSED"
            
        if self.__trade_type == 's':
            return volume_taken
            
        elif self.__trade_type == 'b':
            return volume_taken
            
    def cancel(self, message=""):
        if self.__context.get_flags() & Backtest.DISPLAY_POSITION_CANCELLED:
            if self.__trade_type == 's':
                print('\033['+str(COLOR_YELLOW)+'m'+"> ORDER WITH ID",self.__identifier,"SELLING",self.__volume,self.__context.get_asset_symbol(),'@',self.__price,self.__context.get_money_symbol(), '\033['+str(COLOR_END)+'m')
                print('\033['+str(COLOR_YELLOW)+'m'+"> Reason:"+message+'\033['+str(COLOR_END)+'m')
                
            elif self.__trade_type == 'b':
                print('\033['+str(COLOR_YELLOW)+'m'+"> ORDER WITH ID",self.__identifier,"BUYING",self.__volume,self.__context.get_asset_symbol(),'@',self.__price,self.__context.get_money_symbol(), '\033['+str(COLOR_END)+'m')
                print('\033['+str(COLOR_YELLOW)+'m'+"> Reason:"+message+'\033['+str(COLOR_END)+'m')
                
        self.__messages.append(message)
        self.__status = "CANCELLED"
        
class Backtest:
    DISPLAY_CUSTOM_DATA = 1
    DISPLAY_SELF_BUY = 2
    DISPLAY_SELF_SELL = 4
    DISPLAY_SELF_TRADES = 6
    DISPLAY_POSITION_CANCELLED = 8
    DISPLAY_SYSTEM_LOG = 16
    
    def __init__(self, asset_pair, money=100, flags=31, loss_limit=50):
        # INIT
        self._flags = flags
        self._init_done = False
        self._asset_pair = asset_pair
        apl = int(len(asset_pair)/2)
        self._asset_symbol = asset_pair[0:apl]
        self._money_symbol = asset_pair[apl:apl*2]
        self._trades_history = []
        self._value_max_length = 8
        self._portefolio  = {
            self._money_symbol : money,
            self._asset_symbol : 0
        }
        self._time_sleep = 0
        self._loss_limit = loss_limit
        self._max_ma_length = 0
        self._max_money = money
        self._min_money = money
        self._end_log_line_with = '\n'
        
        # CONTEXT
        self._market_event_index = 0
        self._last_market_event_timestamp = 0 # must be refactored to __last_timestamp
        self._last_buy_price = 0
        self._last_sell_price = 0
        self._last_price = 0
        
        # INDICATOR
        self._ema = {}
        self._macd = {}
        self._sma = {}
        self._ohlcv = {}
        self._rsi = {}
        self._rsi_initialized = False
        
        # PERFORMANCE RELATED
        self._good_transactions_count = 0
        self._bad_transactions_count = 0        
        self._max_return = 0
        self._return = 0
        
        self._extra_data_to_log_callbacks = []      
        
        self._positions = []

    def _bad_transaction(self):
        self._bad_transactions_count += 1

    def _compute_ema(self):
        #[Closing price-EMA (previous day)] x multiplier + EMA (previous day)
        for l in self._ema.keys():
            alpha = 2 / (1 + l)
            for tf in self._ohlcv.keys():
                timestamp = self._ohlcv[tf][-1][0]
                closing_price = self._ohlcv[tf][-1][4]
                current_list_length = len(self._ema[l][tf])
                if current_list_length > 0:
                    current_ema = self._ema[l][tf][-1]
                    if timestamp == current_ema[0] and current_list_length == 1:
                        self._ema[l][tf][-1] = [timestamp, closing_price]
        
                    if timestamp == current_ema[0] and len(self._ema[l][tf]) > 2:
                        previous_ema = self._ema[l][tf][-2][1]
                        current_ema[1] = (closing_price * alpha) + ( previous_ema * (1-alpha) )  
                    
                    else:
                        previous_ema = self._ema[l][tf][-1][1]
                        self._ema[l][tf].append([
                            timestamp,
                            (closing_price * alpha) + ( previous_ema * (1-alpha))
                        ])
                            
                        self._ema[l][tf] = self._ema[l][tf][-self._max_ma_length:]
                        
                else:
                    self._ema[l][tf].append([timestamp, closing_price])

    def _compute_macd(self):
        pass
        #~ for args in self._macd.keys():
            #~ s_ema, l_ema, signal_length = args
            #~ for tf in self._ohlcv.keys():
                #~ timestamp = self._ohlcv[tf][-1][0]
                #~ macd = self._ema[s_ema][tf][-1] - self._ema[l_ema][tf][-1]
                #~ if len(self._macd[l][tf]):
                    #~ if self._macd_initialized and
                    #~ current
                    
                    #~ alpha = 2 / (1 + signal_length)
                    #~ previous_signal = self._macd[l][tf][-1][1]
                    #~ signal = macd * alpha + (previous_signal * (1 - alpha))
                    
                #~ else:
                    #~ signal = macd
                    #~ self._macd[l][tf].append([timestamp, macd, signal])
                    #~ self._macd_initialized = True
                    #~ self._ema[l][tf] = self._ema[l][tf][-self._max_ma_length:]
                
    def _compute_ohlc(self):
        for tf in self._ohlcv.keys():
            price, volume, timestamp = self._trades_history[-1][0:3]                
            timekey = get_time_key(timestamp, tf)
            if len(self._ohlcv[tf]) and timekey == self._ohlcv[tf][-1][0]:
                tohlcv = self._ohlcv[tf][-1]
                self._ohlcv[tf][-1] = [timekey]+synth_ohlcv(price, volume, current=tohlcv[1:])
            
            else:
                self._ohlcv[tf].append([timekey]+synth_ohlcv(price, volume))
                self._ohlcv[tf] = self._ohlcv[tf][-self._max_ma_length:]

    def _compute_rsi(self):
        for l in self._rsi.keys():
            for tf in self._ohlcv.keys():
                ohlcv = self._ohlcv[tf][-1]
                timestamp = ohlcv[0]
                close_price = ohlcv[4]
                    
                if len(self._ohlcv[tf]) >= 2:
                    close_price_old = self._ohlcv[tf][-2][4]
                    
                else:
                    if not len(self._rsi[l][tf]):
                        self._rsi[l][tf].append([timestamp, None, None, None, None, None])
                        
                    return
                    
                gain = close_price - close_price_old if close_price > close_price_old else 0
                loss = close_price_old - close_price if close_price <= close_price_old else 0
                
                current_rsi = self._rsi[l][tf][-1]
                if current_rsi[0] == timestamp:
                    current_rsi[1] = gain
                    current_rsi[2] = loss
                    
                else:
                    current_rsi = [timestamp, gain, loss, None, None, None]
                    self._rsi[l][tf].append(current_rsi)
                    
                if len(self._rsi[l][tf]) > l and (not self._rsi_initialized):
                    window = self._rsi[l][tf][-l:]
                    window_g = [v[1] for v in window]
                    ag = sum(window_g) / l                    
                    window_l = [v[2] for v in window]
                    al = sum(window_l) / l

                    current_rsi[3] = ag
                    current_rsi[4] = al                  
                    current_rsi[5] = 100 - (100 / ((ag/al) + 1))
                    self._rsi_initialized = True
                    
                elif self._rsi_initialized:
                    previous_rsi = self._rsi[l][tf][-2]
                    if previous_rsi[3] != None:
                        ag = (previous_rsi[3] * (l-1) + gain) / l
                        al = (previous_rsi[4] * (l-1) + loss) / l
                        
                    else:
                        window_g = [v[1] for v in self._rsi[l][tf][-l:]]
                        ag = sum(window_g) / l
                        window_l = [v[2] for v in self._rsi[l][tf][-l:]]
                        al = sum(window_l) / l
                            
                    current_rsi = self._rsi[l][tf][-1]
                    current_rsi[3] = ag
                    current_rsi[4] = al
                    current_rsi[5] = 100 - (100 / ((ag/al) + 1))
                    
                self._rsi[l][tf] = self._rsi[l][tf][-self._max_ma_length:]

    def _compute_sma(self):
        for l in self._sma.keys():
            for tf in self._ohlcv.keys():
                timestamp = self._ohlcv[tf][-1][0]
                window = [tohlcv[4] for tohlcv in self._ohlcv[tf][-l:]]
                if len(self._sma[l][tf]) and timestamp == self._sma[l][tf][-1][0]:
                    sma = self._sma[l][tf][-1][1] = sum(window) / len(window)
                    
                else:
                    self._sma[l][tf].append([
                        timestamp,
                        sum(window) / len(window)
                    ])
                    self._sma[l][tf] = self._sma[l][tf][-self._max_ma_length:]

    def _good_transaction(self):
        self._good_transactions_count += 1

    def _perform_buy_if_any(self, position, args):
        price, volume, timestamp, bors = args
        if position.get_trade_type() == 'b' and bors == 's' and price < position.get_price():
            volume_taken = position.update_position(volume)
            self._portefolio[self._asset_symbol] += volume_taken
            self._portefolio[self._money_symbol] -= volume_taken * position.get_price()
            if abs(self._portefolio[self._money_symbol]) < 0.01:
                self._portefolio[self._money_symbol] = 0
                
            if self.DISPLAY_SELF_BUY & self._flags != 0:
                print('\033['+str(COLOR_YELLOW)+'m'+'>', "BUY",volume_taken, self._asset_symbol,"@",position.get_price(),self._money_symbol, '\033['+str(COLOR_END)+'m')        
                print('\033['+str(COLOR_YELLOW)+'m'+'>', "ID:", position.get_identifier(),"VOLUME:",position.get_volume(),"STATUS:", position.get_status(), '\033['+str(COLOR_END)+'m') 
                print('\033['+str(COLOR_YELLOW)+'m'+str(self._portefolio),'\033['+str(COLOR_END)+'m')
                print()
            return True
        return False    
                                
    def _perform_sell_if_any(self, position, args):
        price, volume, timestamp, bors = args
        if position.get_trade_type() == 's' and bors == 'b' and price > position.get_price():
            volume_taken = position.update_position(volume)
            self._portefolio[self._money_symbol] += volume_taken*position.get_price()
            if self._portefolio[self._money_symbol] > self._max_money:
                self._max_money = self._portefolio[self._money_symbol]
                
            if self._portefolio[self._money_symbol] > self._min_money:
                self._min_money = self._portefolio[self._money_symbol]
                
            self._portefolio[self._asset_symbol] -= volume_taken
            if  self.DISPLAY_SELF_SELL & self._flags != 0:
                print('\033['+str(COLOR_YELLOW)+'m'+'>', "SELL",volume_taken, self._asset_symbol,"@",position.get_price(), self._money_symbol, "GET", volume_taken*position.get_price(), self._money_symbol, '\033['+str(COLOR_END)+'m')        
                print('\033['+str(COLOR_YELLOW)+'m'+'>', "ID:", position.get_identifier(),"VOLUME:",position.get_volume(),"STATUS:", position.get_status(), '\033['+str(COLOR_END)+'m') 
                print('\033['+str(COLOR_YELLOW)+'m'+str(self._portefolio),'\033['+str(COLOR_END)+'m')
                print()
            return True
                
        return False

    def _perform_trades(self):
        price, volume, timestamp, bors = self._trades_history[-1]
        for position in self.yield_open_positions():
            if not position.is_expired(timestamp):
                if self._perform_buy_if_any(position, args):
                    continue
                    
                if self._perform_sell_if_any(position, args):
                    continue

    def _print_custom_data(self):
        if (self._flags & Backtest.DISPLAY_CUSTOM_DATA) == 0:
            return

        # GET EXTRA DATA
        extra_data = []
        for value, opt in [ (callback(*args), opt) for callback, args , opt in self._extra_data_to_log_callbacks]:
            string_len, color = opt
            
            if color == 'market' and self.get_current_trade_direction() == 'b':
                color = COLOR_GREEN
                
            elif color == 'market' and self.get_current_trade_direction() == 's':
                color = COLOR_RED

            if string_len >= 1:
                d = str(value)[0:string_len]
                dl = len(d) - string_len
                string = d + ' ' * (-dl) if dl < 0 else d
            else:
                string = str(value)
                
            extra_data.append('\033['+str(color)+'m'+string+'\033[0m')
            
        if len(extra_data):
            print(*tuple(extra_data), end=self._end_log_line_with)

    def _update_trades_history(self, d):
        self._trades_history.append(d)
        if len(self._trades_history) > self._max_ma_length:
            self._trades_history[1:] = self._trades_history[1:]
        
    def _yield_new_data(self):
        data = json.load(open(self._asset_pair+'/trades.json', 'r'))
        for i in range(0, len(data)):
            yield data[i][:4]
            
        if Backtest.DISPLAY_SYSTEM_LOG & self._flags:
            print(COLOR_YELLOW+"> END OF SIMULATION."+COLOR_END)
             
        return None
        
    def add_extra_data_to_log_callback(self, callback, args, opt):
        self._extra_data_to_log_callbacks.append([callback, args, opt])
        
    def add_ohlcv(self, timeframe):
        self._ohlcv[timeframe] = []
        
        for key in self._sma.keys():
            self._sma[key][timeframe] = []
            
        for key in self._ema.keys():
            self._ema[key][timeframe] = []

        for key in self._rsi.keys():
            self._rsi[key][timeframe] = []
            
        for key in self._macd.keys():
            self._macd[key][timeframe] = []

    def add_position(self, price, volume, trade_type, stop_loss = None, identifier=time.time(), valid_for_n_minutes=52560000, notify=True):
        self._positions.append(
            Position(
                self,
                price,
                volume,
                trade_type,
                stop_loss,
                identifier,
                valid_for_n_minutes,
                self._flags & Backtest.DISPLAY_SELF_BUY if trade_type == 'b' else self._flags & Backtest.DISPLAY_SELF_SELL
            )
        )
        return identifier

    def _add_ma(self, ma, l):
        if l <= 0:
            raise ValueError

        if l > self._max_ma_length:
            self._max_ma_length = l
        
        d = {}
        for timeframe in self._ohlcv.keys():
            d[timeframe] = []
                        
        ma[l] = d

    def add_macd(self,short_ema_length, long_ema_length, macd_signal_length):
        self.add_ema(short_ema_length)
        self.add_ema(long_ema_length)
        if macd_signal_length > self._max_ma_length:
            self._max_ma_length = macd_signal_length

        d = {}
        for timeframe in self._ohlcv.keys():
            d[timeframe] = []
            
        self._macd[(short_ema_length, long_ema_length, macd_signal_length)] = d
        
    def add_ema(self, l):
        self._add_ma(self._ema, l)

    def add_rsi(self, l):
        if l <= 0:
            raise ValueError

        if l > self._max_ma_length:
            self._max_ma_length = l
            
        # <time> <Gain> <loss> <AG> <AL> <RSI>
        d = {}
        for timeframe in self._ohlcv.keys():
            d[timeframe] = []
            
        self._rsi[l] = d
            
    def add_sma(self, l):
        self._add_ma(self._sma, l)

    def algo(self):
        print("Not implemented")
                
    def get_asset_pair(self):
        return self._asset_pair
                
    def get_asset_symbol(self):
        return self._asset_symbol

    def get_current_trade(self, offset=0):
        price, volume, t, bors = self._trades_history[-1-offset]
        return (price, volume, timestamp, bors)
        
    def get_flags(self):
        return self._flags

    def get_good_transaction_ratio(self):
        return self._good_transactions_count / (self._good_transactions_count + self._bad_transactions_count)

    def get_current_price(self):
        return self._trades_history[-1][0]

    def get_current_volume(self):
        return self._trades_history[-1][1]

    def get_current_timestamp(self):
        return self._trades_history[-1][2]
                
    def get_current_time(self):
        return datetime.datetime.fromtimestamp(self._trades_history[-1][2])

    def get_current_trade_direction(self):
        return self._trades_history[-1][3]

    def get_current_period(self, timeframe):
        # <time> <open> <high> <low> <close> <volume>
        return datetime.datetime.fromtimestamp(self._ohlcv[timeframe][-1][0])

    def get_current_period_opening_price(self, timeframe):
        # <time> <open> <high> <low> <close> <volume>       
        return self._ohlcv[timeframe][-1][1]
        
    def get_current_period_highest_price(self, timeframe):
        # <time> <open> <high> <low> <close> <volume>
        return self._ohlcv[timeframe][-1][2]

    def get_current_period_lowest_price(self, timeframe):
        # <time> <open> <high> <low> <close> <volume>
        return self._ohlcv[timeframe][-1][3]
        
    def get_current_period_closing_price(self, timeframe):
        # <time> <open> <high> <low> <close> <volume>
        return self._ohlcv[timeframe][-1][4]

    def get_current_period_volume(self, timeframe):
        # <time> <open> <high> <low> <close> <volume>
        return self._ohlcv[timeframe][-1][5]
                
    def get_current_rsi_gain(self, l, tf):
        return self._rsi[l][tf][-1][1]

    def get_current_rsi_loss(self, l, tf):
        return self._rsi[l][tf][-1][2]

    def get_current_rsi_average_gain(self, l, tf):
        return self._rsi[l][tf][-1][3]

    def get_current_rsi_average_loss(self, l, tf):
        return self._rsi[l][tf][-1][4]

    def get_current_rsi(self, l, tf):
        return self._rsi[l][tf][-1][5]
                    
    def get_max_money(self):
        return self._max_money

    def get_min_money(self):
        return self._min_money
    
    def get_money_symbol(self):
        return self._money_symbol
        
    def get_open_position_by_id(self,identifier):
        for position in self._positions:
            if position.get_identifier() == identifier and position.get_status() in ["OPEN", "PARTIAL"]:
                return position

    def get_period_length(self, timeframe):
        return len(self._ohlcv[timeframe])
        
    def get_portefolio_value(self, key):
        return self._portefolio[key]
        
    def get_position_numbers(self):
        return len(self._positions)
        
    def get_sma(self, i, key, offset=0):
        return self._sma[i][key][-1-offset][1]

    def has_open_positions(self):
        return len([position for position in self._positions if position.get_status() in ["OPEN", "PARTIAL"]])

    def rewrite_log_line(self, arg):
        if arg:
            self._end_log_line_with = '\r'
        else:
            self._end_log_line_with = ''
        
    def run(self):
        for d in self._yield_new_data():            
            self._update_trades_history(d)
            
            # COMPUTE INDICATORS IF ANY
            
            self._compute_ohlc()
            self._compute_sma()     
            self._compute_ema()
            self._compute_rsi()
            self._compute_macd()
            
            # LOG
            self._print_custom_data()
            
            # PERFORM TRADE IF ANY
            self._perform_trades()
            # ALGO
            self.algo()
            self.sleep()
            
            self._last_price = self.get_current_price()
            if self._portefolio[self._money_symbol] + self._portefolio[self._asset_symbol] * self._last_price < self._loss_limit:
                print(COLOR_RED+"GAME OVER..."+COLOR_END)
                break
                
            if self._portefolio[self._money_symbol] < 0:
                raise ValueError(str(self._portefolio))

    def set_time_sleep(self, t):
        self._time_sleep = t
        
    def sleep(self):
        time.sleep(self._time_sleep)

    def yield_open_positions(self):
        for position in self._positions:
            if position.get_status() in ["OPEN", "PARTIAL"]:
                yield position
