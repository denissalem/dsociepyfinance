#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from statistics import mean 

def compute_rsi(ohlcv_array, period):
    close_prices = [ ohlcv[4] for ohlcv in ohlcv_array]
    variations   = [0] + [ close_prices[i] - close_prices[i-1] for i in range(1, len(close_prices)) ]

    gains  = [  variation if variation > 0 else 0 for variation in variations]
    losses = [ -variation if variation < 0 else 0 for variation in variations]
    
    rsi = []
    for i in range(0, len(variations)):
        avg_gain = mean( gains[i-(period-1) if i-(period-1) >= 0 else 0 :i+1])
        avg_loss = mean(losses[i-(period-1) if i-(period-1) >= 0 else 0 :i+1])

        rs = 100 if avg_loss == 0 else avg_gain / avg_loss
        rsi.append(100 - (100 / (1 + rs)))
        
    return rsi

# TODO: use the following in the above
def compute_local_rsi(ohlcv_array, period):
    close_prices = [ ohlcv[4] for ohlcv in ohlcv_array]
    variations   = [0] + [ close_prices[i] - close_prices[i-1] for i in range(1, len(close_prices)) ]

    gains  = [  variation if variation > 0 else 0 for variation in variations]
    losses = [ -variation if variation < 0 else 0 for variation in variations]

    avg_losses = mean(losses)

    rs = 100 if avg_losses == 0 else mean( gains ) / avg_losses
    return 100 - (100 / (1 + rs))
