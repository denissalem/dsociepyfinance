#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance.defined_constantes import TIMEFRAMES

def synth_ohlcv(price, volume, current=None):
    if current != None:
        if price > current[1]:
            current[1] = price  #H

        if price < current[2]:
            current[2] = price  #L

        current[3] = price      #C
        current[4] += volume    #V

    else:
        current = ([price]*4) + [volume]
    
    return current

def synth_ohlcv_from_trades(trades, spreads=None):
    trades_uts = {}
    spreads_uts = {}

    for ut in TIMEFRAMES.keys():
        # <price>, <volume>, <time>, <buy/sell>, <market/limit>, <miscellaneous>
        trades_uts[ut] = {}
        spreads_uts[ut] = {}

        for v in trades:                
            time_key = int((v[2] // ut) * ut)
            if time_key in trades_uts[ut].keys():
                if float(v[0]) > trades_uts[ut][time_key][1]:
                    trades_uts[ut][time_key][1] = float(v[0]) #H

                if float(v[0]) < trades_uts[ut][time_key][2]:
                    trades_uts[ut][time_key][2] = float(v[0]) #L

                trades_uts[ut][time_key][3] = float(v[0]) #C
                trades_uts[ut][time_key][4] += float(v[1]) #V

            else:
                trades_uts[ut][time_key] = [float(v[0]), float(v[0]), float(v[0]), float(v[0]), float(v[1])]
                
        # Fill empty ut
        start_from = int(min(trades_uts[ut].keys()))
        end_at = int(max(trades_uts[ut].keys()))
        
        if spreads != None:
            for v in spreads:
                time_key = int((v[0] // ut) * ut)
                if time_key in spreads_uts[ut].keys():
                    spreads_uts[ut][time_key][1] = max([float(value) for value in v[1:]] + [spreads_uts[ut][time_key][1]])
                    spreads_uts[ut][time_key][2] = min([float(value) for value in v[1:]] + [spreads_uts[ut][time_key][2]])
                    spreads_uts[ut][time_key][3] = (float(v[1]) + float(v[2])) / 2
                else:
                    spreads_uts[ut][time_key] = [
                        (float(v[1]) + float(v[2])) / 2,
                        max([float(value) for value in v[1:]]),
                        min([float(value) for value in v[1:]]),
                        (float(v[1]) + float(v[2])) / 2,
                        0
                    ]
                    
        for time_key in range(start_from, end_at, ut):
            if not time_key in trades_uts[ut].keys():
                if time_key in spreads_uts[ut].keys():
                    print("Match spread timekey:", time_key)
                    trades_uts[ut][time_key] = spreads_uts[ut][time_key]
                    
                else:
                    v = trades_uts[ut][int(time_key-ut)][3]
                    trades_uts[ut][int(time_key)] = [v, v, v, v, 0]
         
    return trades_uts
