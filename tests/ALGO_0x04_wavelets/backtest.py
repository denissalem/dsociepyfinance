#! /usr/bin/python3

import json
import os

import numpy as np
import pywt

from dsociepy.finance.plot import stacked_time_frequency

class Streamer:
    def __init__(self):
      self.MA5_filenames = []
      self.ma5_data = None 

      for filename in os.listdir("history/ma"):
          if "-MA5-" in filename and "2020" in filename:
              self.MA5_filenames.append("history/ma/"+filename)

    def yield_data(self, filenames):
      for filename in sorted(filenames):
          json_data = json.loads(open(filename, "r").read())
          for record in json_data:
              print(filename, record)
              yield record

    def stream_ma5_data(self):
        if self.ma5_data == None:
            self.ma5_data = self.yield_data(self.MA5_filenames)
            
        for record in self.ma5_data:
            return record
            
streamer = Streamer()

WINDOW_SIZE = 8640
t = []
sma = []

while len(t) < WINDOW_SIZE:
    price, volume, timestamp = streamer.stream_ma5_data()
    t.append(timestamp)
    sma.append(price)

frame_index = 0
while frame_index < 90*4:
    normalized = (sma - np.mean(sma)) / np.std(sma)
    dt = np.diff(t).mean()
    wavelet = 'shan1.5-1.0'
    scales = np.arange(1, 288)
    
    cfs, frequencies = pywt.cwt(normalized, scales, wavelet, dt)
    power = abs(cfs) ** 2
    
    stacked_time_frequency(t, sma, power, frequencies,save_fig="frames/{:04x}.png".format(frame_index))
    
    for i in range(0,72):
        price, volume, timestamp = streamer.stream_ma5_data()
        
        del t[0]
        del sma[0]
        
        t.append(timestamp)
        sma.append(price)
        
    frame_index+=1
  
