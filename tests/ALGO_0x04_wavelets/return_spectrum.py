#! /usr/bin/python3

import numpy as np
import pywt
import json
import datetime
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation
from matplotlib.pyplot import savefig
from statistics import mean

source_files = [
  "ADAEUR-2022-06.json",
  "ADAEUR-2022-07.json",
  "ADAEUR-2022-08.json",
  "ADAEUR-2022-09.json"
]

history = []
for filename in source_files:
  history += json.loads(open("history/"+filename, "r").read())

history = sorted(history, key=lambda x:x[2])

UTs = []
WINDOW = []        
current_key = 0

def UT_generator():
    for ut in UTs:
        yield ut
        
GENERATOR = UT_generator()

def stream_UT():
    for trades in GENERATOR:
        return trades
        
for trade in history:
    key = (trade[2] // 3600) * 3600

    if key == current_key:
        UTs[-1].append(trade)
        
    else:
        UTs.append([trade])
        current_key = key

for i in range(0,672//2):
    trades = stream_UT()
    WINDOW.append(trades)

fig, ax = plt.subplots(nrows=3, ncols=1, sharex=True)

ax[0].get_xaxis().set_ticks([])
ax[0].get_yaxis().set_ticks([])
ax[1].get_xaxis().set_ticks([])
ax[1].get_yaxis().set_ticks([])  
ax[2].get_xaxis().set_ticks([])
ax[2].get_yaxis().set_ticks([])

old_cfs, old_freqs = [], []

def animate(frame_index):
    ax[0].clear()
    ax[1].clear()
    ax[2].clear()

    local_history = []
    for chunk in WINDOW:
        local_history += chunk

    ma60_y = []
    ma60_x = []
    ma360_y = []
    ma360_x = []    
    
    for trades in WINDOW:
        ma60_y.append(
            mean(
                [v[0] for v in trades]
            )
        )
        ma60_x.append((trades[0][2] // 3600)*3600 + 3600)
        
    hours = 0
    chunk = []
    for trades in WINDOW:
        if hours < 6:
            chunk+=trades
            hours+=1
        else:
            ma360_y.append(
                mean(
                    [v[0] for v in chunk]
                )
            )
            ma360_x.append((chunk[-1][2] // (3600))*(3600) + 3600)
            hours = 0 
            chunk = []
            
    WINDOW.pop(0)
    WINDOW.append(stream_UT())
    
    daily_return = []
    current_day_trades = []
    current_day = datetime.datetime.fromtimestamp(local_history[0][2]).strftime("%Y-%m-%d-%H")

    for trade in local_history:
        dt = datetime.datetime.fromtimestamp(trade[2]).strftime("%Y-%m-%d-%H")
        if dt == current_day:
            current_day_trades.append(trade)
        else:
            start = current_day_trades[0][0]
            end = current_day_trades[-1][0]
            daily_return.append(
              (
                datetime.datetime.strptime(current_day,"%Y-%m-%d-%H").timestamp()+3600,
                ((end - start)/start)*100,
              )
            )
            current_day = dt
            current_day_trades = [trade]
        

    ax[0].set_xlim(local_history[0][2], local_history[-1][2])
    ax[0].plot([v[2] for v in local_history], [v[0] for v in local_history], linewidth=0.5)
    ax[0].plot(ma60_x, ma60_y, linewidth=0.5)
    ax[0].plot(ma360_x, ma360_y, linewidth=0.5)

    ax[1].set_xlim(daily_return[0][0], daily_return[-1][0])
    ax[1].plot([v[0] for v in daily_return], [v[1] for v in daily_return], linewidth=0.5)
    ax[1].axhline(0, alpha=0.2, color='#FF0000', linestyle='-', linewidth=0.8)
        
    returns = [v[1] for v in daily_return]
    
    normalized = (returns - np.mean(returns)) / np.std(returns)
    dt = np.diff([v[0] for v in daily_return]).mean()
    wavelet = 'shan1.5-1.0'
    scales = np.arange(1, 256)
    
    cfs, freqs = pywt.cwt(normalized, scales, wavelet, dt)
    global old_cfs
    if len(old_cfs):
        for i in range(0, len(cfs)):
            cfs[i] = np.append(old_cfs[i][1:], cfs[i][-1:])
            
    old_cfs = cfs
    
    power = abs(cfs) ** 2
    levels = [0.03225806451612903, 0.03333333333333333, 0.034482758620689655, 0.03571428571428571, 0.037037037037037035, 0.038461538461538464, 0.04, 0.041666666666666664, 0.043478260869565216, 0.045454545454545456, 0.047619047619047616, 0.05, 0.05263157894736842, 0.05555555555555555, 0.058823529411764705, 0.0625, 0.06666666666666667, 0.07142857142857142, 0.07692307692307693, 0.08333333333333333, 0.09090909090909091, 0.1, 0.1111111111111111, 0.125, 0.14285714285714285, 0.16666666666666666, 0.2, 0.25, 0.3333333333333333, 0.5, 0.75, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    
    ax[2].invert_yaxis()
    ax[2].contourf(
        [v[0] for v in daily_return],
        np.log2(1./freqs),
        np.log2(abs(cfs)**2),
        np.log2(levels),
        extend='both'
    )
    
    savefig("frames/{:04x}.png".format(frame_index), transparent=False, dpi='figure', format="png",
      metadata=None, bbox_inches=None, pad_inches=0.1,
      facecolor='auto', edgecolor='auto', backend=None
    )
     

ani = animation.FuncAnimation(fig, animate, interval=1)
plt.tight_layout()
plt.show()   

