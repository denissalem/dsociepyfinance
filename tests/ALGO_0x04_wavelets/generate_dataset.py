#! /usr/bin/python3

import os
import datetime
import json
import traceback

import dsociepy.finance.stream as ds_finance_stream


# TRADES : [ Prices, volume, timestamp, action, type ]

ASSET_PAIR = "ADAEUR"
PATH = os.path.expanduser("~/SanDisk3DNAND/works/dsociepyfinance/tests/"+ASSET_PAIR+"/raw/Trades")

PERIODS = {}
streamer = ds_finance_stream.LocalDataStreamer(PATH)

while True:
    try:
        trade = streamer.stream_trades()
        t = str(datetime.datetime.fromtimestamp(trade[2]))
        key = '-'.join(t.split(' ')[0].split('-')[:2])
        if not key in PERIODS.keys():
            PERIODS[key] = [trade]
            if len(PERIODS.keys()) > 8:
                oldest_key = sorted(PERIODS.keys())[0]
                dump = json.dumps(PERIODS[oldest_key])
                if dump != '' or len(PERIODS[oldest_key]) != 0:
                    print("Writing", "history/"+ASSET_PAIR+"-"+oldest_key+".json with", len(PERIODS[oldest_key]), "trades.")
                    f = open("history/"+ASSET_PAIR+"-"+oldest_key+".json", 'w')
                    f.write(dump)
                    f.close()      
                              
                del PERIODS[oldest_key]
                
        else:
            PERIODS[key].append(trade)
            
    except IndexError as e:
        print(e)
        print(traceback.format_exc())
        break
    
