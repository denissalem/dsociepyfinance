#! /usr/bin/python3

# SHOULD BE REUSED OR BE PART OR LIBRARY FOR DATA GENERATION

import json
import os
import datetime
import statistics 

UTs = {
  5 : {},
  15 : {},
  60 : {},
  360 : {},
  1440 : {}
}

previous_data_chunk = []
previous_data_chunk_length = 0
current_data_chunk = []
current_data_chunk_length = 0

for ut in UTs.keys():
    for f in sorted(os.listdir("history")):
        if not os.path.isdir("history/"+f):
            json_file = open("history/"+f)
            
            previous_data_chunk = current_data_chunk
            previous_data_chunk_length = len(current_data_chunk)
            
            current_data_chunk = json.load(json_file)
            current_data_chunk_length = len(current_data_chunk)
          
            data = previous_data_chunk + current_data_chunk
            
            time_key = (int(current_data_chunk[0][2]) // (ut*60)) * (ut*60)
            filename_key = datetime.datetime.fromtimestamp(int(current_data_chunk[0][2])).strftime("%Y-%m")
            UTs[ut][filename_key] = []
            m = []

            for index in range(previous_data_chunk_length, len(data)):
                trade = data[index]
                
                if time_key <= trade[2] < time_key + (ut*60):
                    m.append(
                        (
                          data[index][0], 
                          data[index][1], 
                        )
                    )
                else:
                    if len(m):
                        mean = (
                          statistics.mean([ v[0] for v in m]),                        
                          statistics.mean([ v[1] for v in m]),
                          time_key
                        )
                    else:
                        mean = UTs[ut][filename_key][-1]
                            
                    time_key += ut*60
                    m.clear()
                    
                    print(f, index, '/', len(data), end='\r')
                    UTs[ut][filename_key].append(mean)
                    
            json_file.close()

            print()
            output_filename = "history/ma/ADAEUR-MA"+str(ut)+"-"+filename_key+".json"
            print("saving:", output_filename)
            open(output_filename,"w").write(json.dumps(UTs[ut][filename_key]))
  
