#! /usr/bin/env python3

import os
import json
import random

from collections import deque
from collections import namedtuple
from statistics import mean

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation
from matplotlib.pyplot import savefig

from dsociepy.finance.rsi import compute_rsi
from dsociepy.finance.rsi import compute_local_rsi
from dsociepy.finance.helpers import print_history

CHUNK = 60 # minutes
WINDOW = 24 * CHUNK
SPEED = 60
MAX_TRADE_LIMIT = 2000
FOUR_WEEKS = 4*7*24*60*60

DT = 180
FS = 1/DT  # Fréquence d'échantillonnage en Hz

Trade = namedtuple(
  "Trade",
  "timestamp action portefolio_value saved price trade_rate stop_loss"
)

StopLossTrigger = namedtuple(
  "StopLossTrigger",
  "timestamp action price"
)

def period_trend(short_period, long_period):
  uptrend = 0
  for i in range(0, len(short_period)):
      uptrend += (short_period[i] - long_period[i]) * (i/len(short_period))
  
  return True if uptrend > 0 else False

class Performances:
    def __init__(self, ):
        self.timestamps = []
        self.portefolio = []
        self.market = []
        self.euros = []
        self.saved = []

class Strategy:
    # TODO : https://www.lynxbroker.fr/bourse/trading/analyse-technique/indicateurs-techniques/indicateur-rsi/
    # Filtrer les signaux de rupture de phase avec  l'indicateur RSI

    def __init__(self, portefolio_value):
        self.initial_value = portefolio_value
        self.assets = 0
        self.euros = portefolio_value
        self.last_euros = portefolio_value
        self.saved = 0
        self.trades_history = []
        self.phase_breaks_prices = []
        self.phase_breaks_t = []
        self.schedule = None
        self.state_is_buyer = True
        self.trades_history = []
        self.performances = Performances()
        self.stop_loss_triggers = []
        self.stop_loss_factor = 1.05
    
    def brake(self, stream):
        down_area = 0
        up_area = 0
        
        for index in range(0, len(stream.window_prices_open)):
            up_area += stream.window_prices_open[index] - stream.brake_ma[index] if stream.window_prices_open[index] > stream.brake_ma[index] else 0
            down_area += stream.brake_ma[index] - stream.window_prices_open[index] if stream.window_prices_open[index] < stream.brake_ma[index] else 0
            
        return True if down_area > up_area else False
            
    
    def update_schedule(self, timestamp):
        if self.schedule == None:
            self.schedule = timestamp
            return False
            
        elif timestamp - self.schedule >= 3600 * 3:
            self.schedule = timestamp
            return True
            
        else:
            return False
            
    def phases_normalisation(self, phases):
        low  = min([ min(v) for v in phases])
        high = max([ max(v) for v in phases])
        
        normalized_phases = [
            [ (phases[y][x] - low) / (high -  low) for x in range(0, len(phases[y])) ] for y in range(0, len(phases))
        ]
            
        return normalized_phases    
    
    def update_phasebreaks(self, pxx, stream):
        diffs = []
        phases = self.phases_normalisation(pxx)
        
        for index in range(0,len(phases)):
            a = phases[index][-1]
            b = phases[index][-2]
            diffs.append( a - b  if a > b else b - a )
            
        if max(diffs) >= 0.25:
            self.phase_breaks_t.append(stream.window_t[-1])
            self.phase_breaks_prices.append(stream.window_prices_open[-1])
    
        while len(self.phase_breaks_t) and  self.phase_breaks_t[0] < stream.window_t[0]:
            self.phase_breaks_t.pop(0)
            self.phase_breaks_prices.pop(0)
            
    def perform(self, stream):
        if len(self.phase_breaks_t) and self.phase_breaks_t[-1] == stream.window_t[-1] and not self.brake(stream):
            # uglyyyyy
            # TODO : https://stackoverflow.com/questions/7064289/use-slice-notation-with-collections-deque
            uptrend = period_trend(list(stream.short_ma)[-60:], list(stream.long_ma)[-60:])
            overbuy = period_trend(list(stream.window_prices_open)[-60:], list(stream.short_ma)[-60:])
            
            last_know_best_move = stream.get_last_best_move()
    
            # TODO do not perform contradictory action with recent stop loss
            if self.state_is_buyer:
                # On se positionne :
                # - si downtrend et oversell
                # - si on peut prendre de la marge
                if ((not uptrend) and (not overbuy)) or (len(self.trades_history) and self.trades_history[-1].price / self.stop_loss_factor > stream.window_prices_open[-1] ):
                    last_know_best_best_move_allow = not (stream.window_t[-1] - last_know_best_move["timestamp"] < 3600 // 3 and last_know_best_move["type"] == "red")
                    last_trade_allow = True if len(strategy.trades_history) == 0 else not (stream.window_t[-1] - strategy.trades_history[-1].timestamp < 3600 // 3 and strategy.trades_history[-1].action == "red")
                    if last_know_best_best_move_allow and last_trade_allow:
                        self.buy(stream)
                        self.update_performances(stream)

            else:
                # On se positionne :
                # - si uptrend et overbuy
                # - si on peut prendre de la marge
                if (uptrend and overbuy ) or (len(self.trades_history) and self.trades_history[-1].price * self.stop_loss_factor < stream.window_prices_open[-1]):
                    last_know_best_best_move_allow = not ( stream.window_t[-1] - last_know_best_move["timestamp"] < 3600 // 3 and last_know_best_move["type"] == "green" )
                    last_trade_allow = True if len(strategy.trades_history) == 0 else not (stream.window_t[-1] - strategy.trades_history[-1].timestamp < 3600 // 3 and strategy.trades_history[-1].action == "green")
                    if last_know_best_best_move_allow and last_trade_allow:
                        self.sell(stream)
                        self.update_performances(stream)
            
    def update_performances(self, stream, stop_loss=False):
        self.performances.timestamps.append(stream.window_t[-1])
        self.performances.portefolio.append(
            self.euros + self.assets * stream.window_prices_open[-1]
        )
        self.performances.market.append(stream.window_prices_open[-1])
        self.performances.euros.append(self.last_euros)
        self.performances.saved.append(self.saved)

    def buy(self, stream, stop_loss=None):        
        price = stream.window_prices_open[-1] if stop_loss == None else stop_loss
        
        self.trades_history.append(
            Trade(
                timestamp=stream.window_t[-1],
                action="green",
                portefolio_value=self.euros,
                saved=self.saved,
                price=price,
                trade_rate= ((self.trades_history[-1].price / price) -1)*100 if len(self.trades_history) else 0,
                stop_loss= True if stop_loss != None else False
            )
        )        
        
        self.assets = self.euros / price
        self.euros = 0
        self.state_is_buyer ^= True
        print_history(self.trades_history)
        if stop_loss == None:
            self.push_stop_loss(stream.window_t[-1], "red", price)
        else:
            self.stop_loss_triggers.clear()

    def sell(self, stream, stop_loss=None):    
        price = stream.window_prices_open[-1] if stop_loss == None else stop_loss
    
        self.euros = self.assets * price
        self.last_euros = self.euros
    
        limit_reached = self.euros >= MAX_TRADE_LIMIT
        
        if limit_reached:
            self.saved += self.euros - MAX_TRADE_LIMIT
            self.euros = MAX_TRADE_LIMIT
        
        self.trades_history.append(
            Trade(
                timestamp=stream.window_t[-1],
                action="red",
                portefolio_value=self.euros,
                saved=self.saved,
                price=price,
                trade_rate=((price / self.trades_history[-1].price)-1)*100 if len(self.trades_history) else 0,
                stop_loss= True if stop_loss != None else False

            )
        )     
        
        self.assets = 0
        self.state_is_buyer ^= True
        print_history(self.trades_history)
        if stop_loss == None :
            self.push_stop_loss(stream.window_t[-1], "green", price)
        else:
            self.stop_loss_triggers.clear()
            
    def push_stop_loss(self, timestamp, action, price):
        self.stop_loss_triggers.append(
            StopLossTrigger(
                timestamp=timestamp,
                action=action,
                price=price * self.stop_loss_factor if action == "green" else price / self.stop_loss_factor
            )
        )
        
class StreamMarketData:
    def __init__(self, asset_pair, ut, short_ma, long_ma, brake_period):
        PATH = os.path.expanduser("~/git/dsociepyfinance/tests/"+asset_pair+"/ohlcv-"+str(ut)+".json")
        self.ohlc = [ v for v in json.load(open(PATH, 'r'))]
        self.time_index = 48*60
        self.short_ma_length = short_ma
        self.long_ma_length = long_ma
        self.brake_ma_length = brake_period
        self.short_ma = None
        self.long_ma = None
        self.brake_ma = None
        self.window_prices_open = None
        self.window_t = None

    # Doesn't provide signal, but help to prevent bad moves
    # TODO : NEED TO OPTIMIZED, IT'S UGLY !!
    def compute_best_reversal_move(self):
        last_reversal_index = 0
        
        signal_indexes = []
        signal_types = []
        for index in range(1, len(self.window_t)):
            if self.short_ma[index] > self.long_ma[index] and self.short_ma[index-1] < self.long_ma[index-1]:
                sub_window = tuple(self.window_prices_open)[last_reversal_index:index]
                signal_indexes.append(last_reversal_index + sub_window.index( min(sub_window) ))
                signal_types.append('green')
                last_reversal_index = index
    
            elif self.short_ma[index] < self.long_ma[index] and self.short_ma[index-1] > self.long_ma[index-1]:
                sub_window = tuple(self.window_prices_open)[last_reversal_index:index]
                signal_indexes.append(last_reversal_index + sub_window.index( max(sub_window) ))
                signal_types.append('red')
                last_reversal_index = index
            
        self.best_known_moves_price = [ self.window_prices_open[i] for i in signal_indexes ]
        self.best_known_moves_t = [ self.window_t[i] for i in signal_indexes ]
        self.best_known_moves_type = signal_types
        
    def get_last_best_move(self):
        if len(self.best_known_moves_t):
            return {
                "timestamp" : self.best_known_moves_t[-1],
                "price": self.best_known_moves_price[-1],
                "type": self.best_known_moves_type[-1]
            }
        else:
            return {
                "timestamp": 0,
                "price": None,
                "type": None
            }
          
    def initialize_MA(self, data, length):
        return deque([ mean(data[i-length if i - length > 0 else 0:i+1]) for i in range(0, len(data)) ], len(data))

    def update_data(self):
        # TODO : https://stackoverflow.com/questions/10003143/how-to-slice-a-deque
        if self.window_t == None and self.window_prices_open == None:
            tohlcv = self.ohlc[self.time_index:self.time_index+WINDOW]       
            window = [ v[:4] for v in tohlcv ]
            self.window_prices_open = deque([ v[1] for v in window ], len(window))
            self.window_prices_high = deque([ v[2] for v in window ], len(window))
            self.window_prices_low =  deque([ v[3] for v in window ], len(window))
            self.window_t = deque([ v[0] for v in window ], len(window))
            tupled_window_prices_open = tuple(self.window_prices_open)
            self.short_ma = self.initialize_MA(tupled_window_prices_open, self.short_ma_length)
            self.long_ma = self.initialize_MA(tupled_window_prices_open, self.long_ma_length)
            self.brake_ma = self.initialize_MA(tupled_window_prices_open, self.brake_ma_length)
            
        else:
            data = self.ohlc[self.time_index+WINDOW][:4]
            self.window_t.append(data[0])
            self.window_prices_open.append(data[1])
            self.window_prices_high.append(data[2])
            self.window_prices_low.append(data[3])
            
            tupled_window_prices_open = tuple(self.window_prices_open)

            self.short_ma.append(mean(tupled_window_prices_open[-self.short_ma_length:]))
            self.long_ma.append(mean(tupled_window_prices_open[-self.long_ma_length:]))
            self.brake_ma.append(mean(tupled_window_prices_open[-self.brake_ma_length:]))
            self.time_index += 1
        
        self.compute_best_reversal_move()

def animate(frame_index):
    for i in range(0,SPEED):
        stream.update_data()
        
        # Process Stop losses
        if len(strategy.stop_loss_triggers):
            if strategy.stop_loss_triggers[-1].action == "green" and stream.window_prices_high[-1] > strategy.stop_loss_triggers[-1].price:
                strategy.buy(stream, stop_loss=strategy.stop_loss_triggers[-1].price)
                strategy.update_performances(stream)

              
            elif strategy.stop_loss_triggers[-1].action == "red" and stream.window_prices_low[-1] < strategy.stop_loss_triggers[-1].price:
                strategy.sell(stream, stop_loss=strategy.stop_loss_triggers[-1].price)
                strategy.update_performances(stream)

            # make stop_loss follow
            elif strategy.stop_loss_triggers[-1].action == "green" and stream.window_prices_high[-1] * strategy.stop_loss_factor < strategy.stop_loss_triggers[-1].price:
                strategy.stop_loss_triggers[-1] = StopLossTrigger(
                    timestamp=strategy.stop_loss_triggers[-1].timestamp,
                    action=strategy.stop_loss_triggers[-1].action,
                    price=stream.window_prices_high[-1] * strategy.stop_loss_factor
                )
                
            elif strategy.stop_loss_triggers[-1].action == "red" and stream.window_prices_low[-1] / strategy.stop_loss_factor > strategy.stop_loss_triggers[-1].price:
                strategy.stop_loss_triggers[-1] = StopLossTrigger(
                    timestamp=strategy.stop_loss_triggers[-1].timestamp,
                    action=strategy.stop_loss_triggers[-1].action,
                    price=stream.window_prices_high[-1] / strategy.stop_loss_factor
                )

        # Apply strategy
        if strategy.update_schedule(stream.window_t[-1]):
            # PHASE SPECTROGRAM
            ax2.clear()
            Pxx, freqs, bins, im = ax2.specgram(list(stream.window_prices_open), NFFT=DT, Fs=FS, noverlap=0, mode='phase')
            strategy.update_phasebreaks(Pxx, stream)
            strategy.perform(stream)

    # ~ # PRICES
    ax1.clear()
    for loc in range(int(stream.window_t[0]), int(stream.window_t[-1]), ((int(stream.window_t[-1]) - int(stream.window_t[0]))//24)*3):
        ax1.axvline(loc, alpha=0.2, color='#808080', linestyle='-', linewidth=0.8)
        
    ax1.set_xlim([stream.window_t[0], stream.window_t[-1]])
    ax1.set_ylim([min(stream.window_prices_open), max(stream.window_prices_open)])
        
    ax1.plot(stream.window_t, stream.window_prices_open, color='black', linewidth = 0.5)
    ax1.plot(stream.window_t, stream.short_ma, color='blue', linewidth = 0.5)
    ax1.plot(stream.window_t, stream.long_ma, color='violet', linewidth = 0.5)
    ax1.plot(stream.window_t, stream.brake_ma, color='red', linewidth = 0.5)
    
    ax1.scatter(
        stream.best_known_moves_t,
        stream.best_known_moves_price,
        marker='x',
        c=stream.best_known_moves_type,
        s=50,
    )
    
    ax1.scatter(
        strategy.phase_breaks_t,
        strategy.phase_breaks_prices,
        marker='o',
        c='black',
        s=50,
    )
    
    # TODO: cache index
    performed_strategy = [ trade for trade in strategy.trades_history if trade.timestamp >= stream.window_t[0] ]

    ax1.scatter(
        [trade.timestamp for trade in performed_strategy],
        [trade.price for trade in performed_strategy],
        marker='o',
        c=[trade.action for trade in performed_strategy],
        s=25,
    )
    
    # PERFORMANCES
    if len(strategy.performances.timestamps):
        max_performances = max(strategy.performances.market)
        ax4.clear()
        ax5.clear()

        ax4.axhline(strategy.initial_value, alpha=0.2, color="red", linestyle='-', linewidth=0.8)
        if max_performances > MAX_TRADE_LIMIT:
            ax4.axhline(MAX_TRADE_LIMIT, color="green", linestyle='-', linewidth=0.8)
        for i in range(0, (strategy.performances.timestamps[-1] - strategy.performances.timestamps[0]) // (FOUR_WEEKS)):         
            ax4.axvline(strategy.performances.timestamps[0] + i * (FOUR_WEEKS ), alpha=0.2, color='#808080', linestyle='-', linewidth=0.8)
            ax5.axvline(strategy.performances.timestamps[0] + i * (FOUR_WEEKS ), alpha=0.2, color='#808080', linestyle='-', linewidth=0.8)
     
        ax4.plot(strategy.performances.timestamps, strategy.performances.euros, color='black', linewidth = 0.5)    
        ax4.plot(strategy.performances.timestamps, strategy.performances.saved, color='gold', linewidth = 0.5)    
        ax5.set_ylim([0, max_performances])
        ax5.plot(strategy.performances.timestamps, strategy.performances.market, color='black', linewidth = 0.5)

    plt.tight_layout()

    savefig("frames/{:04x}.png".format(frame_index), transparent=False, dpi='figure', format="png",
      metadata=None, bbox_inches=None, pad_inches=0.1,
      facecolor='auto', edgecolor='auto', backend=None
    )
    
strategy = Strategy(200)

stream = StreamMarketData("ADAEUR", 60, 64, 256, 1024)

#https://stackoverflow.com/questions/41025187/matplotlib-add-subplot-odd-number-of-plots

fig = plt.figure(figsize=(6, 4))
gs = gridspec.GridSpec(nrows=6, ncols=2, )
gs.tight_layout(fig)

ax1 = fig.add_subplot(gs[:4, 0])
ax2 = fig.add_subplot(gs[4:, 0])
ax4 = fig.add_subplot(gs[:3, 1])
ax5 = fig.add_subplot(gs[3:, 1])

ani = animation.FuncAnimation(fig, animate, interval=25)

plt.show()
