#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import json
from dsociepy.finance.ohlcv import synth_ohlcv_from_trades

print("Loading reference data...")

trades = open('input_trades_data.json','r').read()
trades = json.loads(trades)
previous_tk = (trades[0][2] // 300) * 300
tk_count = 0
trades_limited = []
for trade in trades:
    if previous_tk != (trade[2] // 300) * 300:
        tk_count += 1
        previous_tk = (trade[2] // 300) * 300
        
    if tk_count >= 1000:
        break
        
    trades_limited.append(trade)

ohlcv = synth_ohlcv_from_trades(trades_limited)
rsi = {}
for ut in ohlcv.keys():
    rsi[ut] = []
    for tk in sorted(ohlcv[ut].keys()):
        rsi[ut].append([ohlcv[ut][tk][3]] + [None]*5)
    
    rsi[ut][0][2] = 0
    rsi[ut][0][3] = 0        
    for i in range(1, len(rsi[ut])):
        if rsi[ut][i][0] > rsi[ut][i-1][0]:
           rsi[ut][i][1] = rsi[ut][i][0] - rsi[ut][i-1][0]
           rsi[ut][i][2] = 0
           
        elif rsi[ut][i][0] < rsi[ut][i-1][0]:
           rsi[ut][i][1] = 0
           rsi[ut][i][2] = rsi[ut][i-1][0] - rsi[ut][i][0]
        
        else:
           rsi[ut][i][1] = 0
           rsi[ut][i][2] = 0
    
    for i in range(14, len(rsi[ut])):
        if i == 14:
            rsi[ut][i][3] = sum([ v[1] for v in rsi[ut][i-14+1:i+1] ]) / 14
            rsi[ut][i][4] = sum([ v[2] for v in rsi[ut][i+1-14:i+1] ]) / 14

        else:
            rsi[ut][i][3] = (rsi[ut][i-1][3] * (13) + rsi[ut][i][1]) / 14
            rsi[ut][i][4] = (rsi[ut][i-1][4] * (13) + rsi[ut][i][2]) / 14
            
        if i == 14:               
            rsi[ut][i][5] = 100 - (100 / ((rsi[ut][i][3]/rsi[ut][i][4]) + 1))
            
        else:
            sg = (rsi[ut][i-1][3] * 13 + rsi[ut][i][1]) / 14
            sl = (rsi[ut][i-1][4] * 13 + rsi[ut][i][2]) / 14
            if sl == 0 and sg != 0:
                rsi[ut][i][5] = 100
                
            elif sl == 0 and sg == 0:
                rsi[ut][i][5] = 0
                
            else:
                rsi[ut][i][5] = 100 - (100 / ((sg/sl) + 1))

print("Done.")
