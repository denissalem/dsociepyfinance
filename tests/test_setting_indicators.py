#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance import Backtest, Position, synth_ohlcv
import time

expected_values = {
	'sma' : [9,18,27],
	'ema' : [10,19,30],
	'rsi' : [14,28],
	'macd': [(1,2,20) , (128,256,512)],
	'ohlcv' : [60, 900, 3600]
}

class TestBacktest(Backtest):
	def get_ema_args_list(self):
		return self._ema.keys()

	def get_sma_args_list(self):
		return self._sma.keys()

	def get_rsi_args_list(self):
		return self._rsi.keys()

	def get_macd_args_list(self):
		return self._macd.keys()
			
	def get_sma_list(self):
		return [ self._sma[key] for key in self._sma.keys()]

	def get_rsi_list(self):
		return [ self._rsi[key] for key in self._rsi.keys()]
		
	def get_ema_list(self):
		return [ self._ema[key] for key in self._ema.keys()]

	def get_macd_list(self):
		return [ self._macd[key] for key in self._macd.keys()]
		
def setting(bt, indicator):
	setter = getattr(bt, "add_"+indicator)
	for v in expected_values[indicator]:
		if type(v) == tuple:
			setter(*v)
		else:
			setter(v)

def test_expected_values(bt, indicators_involved):
	for indicator in indicators_involved:
		getter_length_list = getattr(bt, "get_"+indicator+"_args_list")
		if len(getter_length_list()) == 0:
			return False
			
		for l in expected_values[indicator]:
			if not l in getter_length_list():
				return False
	
		getter_list = getattr(bt, "get_"+indicator+"_list")
		if len(getter_list()) == 0:
			return False
			
		for indicator in getter_list():
			for tf in expected_values["ohlcv"]:
				if not tf in indicator.keys(): 
					return False
					
	return True
	
def setting_indicators(indicators):
	bt = TestBacktest("ADAEUR")
	for indicator in indicators:
		setting(bt, indicator)
	return test_expected_values(bt, [i for i in indicators if i != "ohlcv"])

# INDICATORS SETTING
assert(setting_indicators(["sma",   "ohlcv"]))
assert(setting_indicators(["ohlcv", "sma"]))
assert(setting_indicators(["rsi",   "ohlcv"]))
assert(setting_indicators(["ohlcv", "rsi"]))
assert(setting_indicators(["ema",   "ohlcv"]))
assert(setting_indicators(["ohlcv", "ema"]))
assert(setting_indicators(["macd",   "ohlcv"]))
assert(setting_indicators(["ohlcv", "macd"]))

assert(setting_indicators(["sma", "rsi", "ohlcv"]))
assert(setting_indicators(["rsi", "sma", "ohlcv"]))
assert(setting_indicators(["sma", "ohlcv", "rsi"]))
assert(setting_indicators(["ohlcv", "sma", "rsi"]))
assert(setting_indicators(["rsi", "ohlcv", "sma"]))
assert(setting_indicators(["ohlcv", "rsi", "sma"]))

assert(setting_indicators(["macd", "rsi", "ohlcv"]))
assert(setting_indicators(["rsi", "macd", "ohlcv"]))
assert(setting_indicators(["macd", "ohlcv", "rsi"]))
assert(setting_indicators(["ohlcv", "macd", "rsi"]))
assert(setting_indicators(["rsi", "ohlcv", "macd"]))
assert(setting_indicators(["ohlcv", "rsi", "macd"]))

assert(setting_indicators(["macd", "sma", "ohlcv"]))
assert(setting_indicators(["sma", "macd", "ohlcv"]))
assert(setting_indicators(["macd", "ohlcv", "sma"]))
assert(setting_indicators(["ohlcv", "macd", "sma"]))
assert(setting_indicators(["sma", "ohlcv", "macd"]))
assert(setting_indicators(["ohlcv", "sma", "macd"]))

assert(setting_indicators(["macd", "ema", "ohlcv"]))
assert(setting_indicators(["ema", "macd", "ohlcv"]))
assert(setting_indicators(["macd", "ohlcv", "ema"]))
assert(setting_indicators(["ohlcv", "macd", "ema"]))
assert(setting_indicators(["ema", "ohlcv", "macd"]))
assert(setting_indicators(["ohlcv", "ema", "macd"]))
