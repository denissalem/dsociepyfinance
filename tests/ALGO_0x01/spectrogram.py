#! /usr/bin/env python3

import os
import json
import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import random
from statistics import mean
from datetime import timedelta

from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from matplotlib.pyplot import savefig

from dsociepy.finance.helpers import print_history

PATH_ADAEUR = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/ohlcv-60.json") # One minute OHLC
OHLC_60 = [ v[:5] for v in json.load(open(PATH_ADAEUR, 'r'))]
SIGNALS = json.load(open("signals.json",'r'))

MA_SCALES = [ json.load(open("MA-"+str(l)+".json", 'r')) for l in [64,256]]

MACD = [ MA_SCALES[0][i] - MA_SCALES[1][i] for i in range(0, len(OHLC_60)) ]

BBP = [ v[1] for v in json.load(open("BBP_60.json","r")) ]
BBP_MA_SCALES = [ json.load(open("MA-BBP-"+str(l)+".json", 'r')) for l in [64,256,512]]

OFFSET = 60*24*30*random.randint(6,36)
SPEED = 60

dt = 60
fs = 1/dt  # Fréquence d'échantillonnage en Hz
duration = 60*60*24
t = np.arange(0, duration, dt)  # Échantillonnage sur 1 seconde
WINDOW = len(t)
NOW = 86400

fig, ((ax1, ax4, ax7), (ax3, ax6, ax9)) = plt.subplots(2, 3, sharex=True, layout='constrained')

nfft = 60

prices_phase_breaks_t = list()
prices_phase_breaks = list()

bbp_phase_breaks_t = list()
bbp_phase_breaks = list()

macd_phase_breaks_t = list()
macd_phase_breaks = list()

phase_signals = list()

local_signals = list()
local_signals_c = list()
local_signals_t = list()

PHASE_INVERSION_COUNTDOWN = list()

BUYER = True

PORTE_FOLIO = {
    "CRYPTOSHITCOIN": 0,
    "EURO": 200
}

TRADE_HISTORY = []
TIME_ELAPSED = 0

def phases_normalisation(phases):
    low  = min([ min(v) for v in phases])
    high = max([ max(v) for v in phases])
    
    normalized_phases = [
        [ (phases[y][x] - low) / (high -  low) for x in range(0, len(phases[y])) ] for y in range(0, len(phases))
    ]
        
    return normalized_phases    
    
def update_phase_breaks(t, signals, phases, price, len_t):
    diffs = []
    for index in range(0,len(phases)):
        a = phases[index][-1]
        b = phases[index][-2]
        diffs.append( a - b  if a > b else b - a )
        
    for i in range(0, len(t)):
        t[i] = t[i] - (len_t*60)//24
    
    if max(diffs) >= 0.25:
        t.append((len_t-1)*60)
        signals.append(price)

    while len(t) and t[0] < 0:
        t.pop(0)
        signals.pop(0)

def phase_breaks_filtering(prices_t, bbp_t, macd_t, prices, bbp, macd):
    return prices_t, prices

def trigger_phase_inversion():
    return True if len(PHASE_INVERSION_COUNTDOWN) == 3 else False
    
def period_trend(short_period, long_period):
  uptrend = 0
  for i in range(0, len(short_period)):
      uptrend += (short_period[i] - long_period[i]) * (i/len(short_period))
  
  return True if uptrend > 0 else False

def buy(STOCK_PRICES):
    global BUYER
    global PORTE_FOLIO
    
    local_signals_t.append(t[-1])
    local_signals_c.append("green")
    local_signals.append(STOCK_PRICES["open"][-1])
    BUYER ^= True
    PORTE_FOLIO["CRYPTOSHITCOIN"] = PORTE_FOLIO["EURO"] / STOCK_PRICES["open"][-1]
    
    TRADE_HISTORY.append({
        "action": "Buy ",
        "portefolio_value": PORTE_FOLIO["EURO"],
        "price" : STOCK_PRICES["open"][-1],
        "trade_rate" : ((TRADE_HISTORY[-1]["price"] / STOCK_PRICES["open"][-1])-1)*100 if len(TRADE_HISTORY) else 0
    })
    PORTE_FOLIO["EURO"] = 0
    print_history(TRADE_HISTORY)
    print("TIME ELAPSED:", timedelta(seconds=TIME_ELAPSED*60))

    PHASE_INVERSION_COUNTDOWN.clear()
    PHASE_INVERSION_COUNTDOWN.append(STOCK_PRICES["open"][-1])
    
def sell(STOCK_PRICES):
    global BUYER
    global PORTE_FOLIO
    
    local_signals_t.append(t[-1])
    local_signals_c.append("red")
    local_signals.append(STOCK_PRICES["open"][-1])
    BUYER ^= True
    PORTE_FOLIO["EURO"] = PORTE_FOLIO["CRYPTOSHITCOIN"] * STOCK_PRICES["open"][-1]
    TRADE_HISTORY.append({
        "action": "Sell",
        "portefolio_value": PORTE_FOLIO["EURO"],
        "price" : STOCK_PRICES["open"][-1],
        "trade_rate" : ((STOCK_PRICES["open"][-1] / TRADE_HISTORY[-1]["price"])-1)*100 if len(TRADE_HISTORY) else 0
    })
    PORTE_FOLIO["CRYPTOSHITCOIN"] = 0
    print_history(TRADE_HISTORY)
    print("TIME ELAPSED:", timedelta(seconds=TIME_ELAPSED*60))
    
    PHASE_INVERSION_COUNTDOWN.clear()
    PHASE_INVERSION_COUNTDOWN.append(STOCK_PRICES["open"][-1])
    
def animate(i):
    global TIME_ELAPSED
    TIME_ELAPSED += 60
    # ~ input()
    ax1.clear()
    ax3.clear()
    ax4.clear()
    ax6.clear()    
    ax7.clear()    
    ax9.clear()    
    
    STOCK_PRICES = {
      "open":  [ v[1] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ],
      "high":  [ v[2] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ],
      "low":   [ v[3] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ],
      "close": [ v[4] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    }
    
    SIGNALS_WINDOW = SIGNALS[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW]
    
    if not len(STOCK_PRICES["open"]):
        print("END_OF_SIMULATON")
        exit()
    
    #####################
    # PROCESS STOP LOSS #
    #####################
    
    #########################
    # END PROCESS STOP LOSS #
    #########################
    
    MA_256 =  [ v for v in MA_SCALES[1][OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    MA_64 =  [ v for v in MA_SCALES[0][OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    ax1.set(
      xlim=(0, len(t)),
      ylim=(min(STOCK_PRICES["open"]), max(STOCK_PRICES["open"]))
    )
    
    ax1.plot(t, STOCK_PRICES["open"], color='black', linewidth = 0.5)    
    ax1.plot(t, MA_64, linewidth = 0.5)
    ax1.plot(t, MA_256, linewidth = 0.5)

    ax1.grid(False, which='both')
    for loc in range(0, len(t)*60, (len(t)*60)//24):
        ax1.axvline(loc, alpha=0.2, color='#b0b0b0', linestyle='-', linewidth=0.8)
        
    ax1.scatter(
        t,
        [s["price"] for s in SIGNALS_WINDOW],
        marker='o',
        c=['red' if s["action"] == 's' else 'green' for s in SIGNALS_WINDOW],
        s=[0 if not s["action"] in ['s','b'] else 20 for s in SIGNALS_WINDOW],
    )
    
    Pxx, freqs, bins, im = ax3.specgram(STOCK_PRICES["open"], NFFT=nfft, Fs=fs, noverlap=0, mode='phase')
    
    update_phase_breaks(
        prices_phase_breaks_t,
        prices_phase_breaks,
        phases_normalisation(Pxx),
        STOCK_PRICES["open"][-1],
        len(t)
    )   
    
    BBP_MA_256 =  [ v for v in BBP_MA_SCALES[1][OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    BBP_MA_512 =  [ v for v in BBP_MA_SCALES[2][OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]

    ax4.grid(False, which='both')
    for loc in range(0, len(t)*60, (len(t)*60)//24):
        ax4.axvline(loc, alpha=0.2, color='#b0b0b0', linestyle='-', linewidth=0.8)
    
    ax4.plot(t, BBP_MA_256)
    ax4.plot(t, BBP_MA_512)
    ax4.set(
      xlim=(0, len(t)),
      ylim=(min(BBP_MA_256), max(BBP_MA_256))
    )
    ax4.axhline(y=0, color='k')
    ax4.scatter(
        t,
        [0 for s in SIGNALS[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW]],
        marker='o',
        c=['red' if s["action"] == 's' else 'green' for s in SIGNALS_WINDOW],
        s=[0 if not s["action"] in ['s','b'] else 40 for s in SIGNALS_WINDOW],
    )
    Pxx, freqs, bins, im = ax6.specgram(BBP_MA_512, NFFT=nfft, Fs=fs, noverlap=0, mode='phase')

    update_phase_breaks(
        bbp_phase_breaks_t,
        bbp_phase_breaks,
        phases_normalisation(Pxx),
        STOCK_PRICES["open"][-1],
        len(t)
    )
        
    MACD_WINDOW = [ v for v in MACD[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    ax7.grid(False, which='both')
    for loc in range(0, len(t)*60, (len(t)*60)//24):
        ax7.axvline(loc, alpha=0.2, color='#b0b0b0', linestyle='-', linewidth=0.8)
        
    ax7.set(
      xlim=(0, len(t)),
      ylim=(min(MACD_WINDOW), max(MACD_WINDOW))
    )
    
    ax7.plot(t, MACD_WINDOW)
    ax7.axhline(y=0, color='k')
    ax7.scatter(
        t,
        [0 for s in SIGNALS[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW]],
        marker='o',
        c=['red' if s["action"] == 's' else 'green' for s in SIGNALS_WINDOW],
        s=[0 if not s["action"] in ['s','b'] else 40 for s in SIGNALS_WINDOW],
    )

    Pxx, freqs, bins, im = ax9.specgram(MACD_WINDOW, NFFT=nfft, Fs=fs, noverlap=0, mode='phase')

    update_phase_breaks(
        macd_phase_breaks_t,
        macd_phase_breaks,
        phases_normalisation(Pxx),
        STOCK_PRICES["open"][-1],
        len(t)
    )

    matchs_t, matchs = phase_breaks_filtering(
        prices_phase_breaks_t,
        bbp_phase_breaks_t,
        macd_phase_breaks_t,
        prices_phase_breaks,
        bbp_phase_breaks,
        macd_phase_breaks,
        #(len(t)*60)//24
    )
    
    #####################
    # SIGNAL PROCESSING #
    #####################
        
    for i in range(0, len(local_signals_t)):
        local_signals_t[i] = local_signals_t[i] - (len(t)*60)//24

    while len(local_signals_t) and local_signals_t[0] < 0:
        local_signals_t.pop(0)
        local_signals_c.pop(0)
        local_signals.pop(0)
            
    if len(matchs_t) and matchs_t[-1] == t[-1]:
        uptrend = period_trend(MA_64[-(len(t)//24):], MA_256[-(len(t)//24):])
        overbuy = period_trend(STOCK_PRICES["open"][-(len(t)//24):], MA_64[-(len(t)//24):])
                    
        global BUYER

        if BUYER:
            # On se positionne :
            # - si downtrend et oversell
            # - si on peut prendre de la marge
            if ((not uptrend) and (not overbuy)) or (len(TRADE_HISTORY) and TRADE_HISTORY[-1]["price"] > STOCK_PRICES["open"][-1]):
                buy(STOCK_PRICES)
            
            else:
              # ~ if uptrend and overbuy:
              if len(PHASE_INVERSION_COUNTDOWN) and PHASE_INVERSION_COUNTDOWN[-1] < STOCK_PRICES["open"][-1]:
                  PHASE_INVERSION_COUNTDOWN.append(STOCK_PRICES["open"][-1])
                  if trigger_phase_inversion():
                      # ~ print("BUYER:", BUYER, STOCK_PRICES["open"][-len(t)//24] > STOCK_PRICES["open"][-1], PHASE_INVERSION_COUNTDOWN)
                      buy(STOCK_PRICES)
                      
                  else:
                      pass
                      # ~ print("BUYER:", BUYER, "inc phase inversion")
                      
              else:
                  # ~ print("BUYER:", BUYER, "Reset phase inversion")
                  PHASE_INVERSION_COUNTDOWN.clear()
                  PHASE_INVERSION_COUNTDOWN.append(STOCK_PRICES["open"][-1])
        else:
            # On se positionne :
            # - si uptrend et overbuy
            # - si on peut prendre de la marge
            if (uptrend and overbuy) or (len(TRADE_HISTORY) and TRADE_HISTORY[-1]["price"] < STOCK_PRICES["open"][-1]):
                sell(STOCK_PRICES)

            else:
              # ~ if (not uptrend) and (not overbuy):
              if len(PHASE_INVERSION_COUNTDOWN) and PHASE_INVERSION_COUNTDOWN[-1] > STOCK_PRICES["open"][-1]:
                  PHASE_INVERSION_COUNTDOWN.append(STOCK_PRICES["open"][-1])
                  if trigger_phase_inversion():
                      # ~ print("BUYER:", BUYER, STOCK_PRICES["open"][-len(t)//24] > STOCK_PRICES["open"][-1], PHASE_INVERSION_COUNTDOWN)
                      sell(STOCK_PRICES)
                  else:
                      pass
                      # ~ print("BUYER:", BUYER, "inc phase inversion")

              else:
                  # ~ print("BUYER:", BUYER, "Reset phase inversion")
                  PHASE_INVERSION_COUNTDOWN.clear()
                  PHASE_INVERSION_COUNTDOWN.append(STOCK_PRICES["open"][-1])
    
    #####################
    #      END OF       #
    # SIGNAL PROCESSING #
    #####################
            
    if len(prices_phase_breaks):
        ax1.scatter(
            matchs_t,
            matchs,
            marker='o',
            c='k',
            s=80
        )
        
    if len(local_signals_t):
        ax1.scatter(
            local_signals_t,
            local_signals,
            marker='o',
            c=local_signals_c,
            s=80
        )
        
    if len(bbp_phase_breaks):
        ax4.scatter(
            matchs_t,
            [0 for v in matchs],
            marker='o',
            c='k',
            s=80
        )
        
    if len(macd_phase_breaks):
        ax7.scatter(
            matchs_t,
            [0 for v in matchs],
            marker='o',
            c='k',
            s=80
        )
        
    savefig("frames/{:04x}.png".format(i), transparent=False, dpi='figure', format="png",
      metadata=None, bbox_inches=None, pad_inches=0.1,
      facecolor='auto', edgecolor='auto', backend=None
    )
        
ani = animation.FuncAnimation(fig , animate, interval=50)

plt.show()
