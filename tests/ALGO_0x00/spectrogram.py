#! /usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

dt = 1/500
fs = 1/dt  # Fréquence d'échantillonnage en Hz

t = np.arange(0, 10, dt)  # Échantillonnage sur 1 seconde
signal = np.sin(2 * np.pi * 5 * t) + np.sin(2 * np.pi * 20 * t) * 0.5
noise  = signal + 0.5 * np.random.randn(len(signal)) * [1 - i / len(signal) for i in range(0, len(signal))]

fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True)
ax1.plot(t, noise)
ax1.set_ylabel('Signal')

Pxx, freqs, bins, im = ax2.specgram(noise, NFFT=250, Fs=fs, noverlap=125)
# The `specgram` method returns 4 objects. They are:
# - Pxx: the periodogram
# - freqs: the frequency vector
# - bins: the centers of the time bins
# - im: the .image.AxesImage instance representing the data in the plot

ax2.set_xlabel('Time (s)')
ax2.set_ylabel('Frequency (Hz)')
ax2.set_xlim(0, 10)
plt.show()
