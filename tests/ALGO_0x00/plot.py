#! /usr/bin/env python3

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os
import json
import datetime
from statistics import mean
import numpy as np
import math

""" TODO
- ADD RSI + RSI POWER SPECTRUM
- ADD SIGNAL RECONSTRUCTION, TRY TO MATCH ORIGINAL IGNAL
- TRY TO FILTER SPECTRUM WITH VALUE ABOVE THRESHOLD
- Remove low frequency to grab real periodic signal
- STUDY PHASES BEHAVIORS
- Hilbert transform
- short-time Fourier transform
- Wigner distributions
- the Radon Transform
"""


from dsociepy.finance.fourier_analysis import power_spectrum_from_dft

from colour import Color
COLORS = list(Color("blue").range_to(Color("red"),6))
LINE_WEIGHT = 1.0
PATH_ADAEUR = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/ohlcv-900.json")

OHLC_900 = json.load(open(PATH_ADAEUR, 'r'))
MA_SCALES = [ json.load(open("MA-"+str(l)+".json", 'r')) for l in [16,32]]
MA_BBP_SCALES = [ json.load(open("MA-BBP-"+str(l)+".json", 'r')) for l in [16,32]]

NOT_BEFORE = datetime.datetime(2021, 1, 10, 0, 0, 0, 0).timestamp()

OFFSET = len([ r[1] for r in OHLC_900 if r[0] < NOT_BEFORE])
print("OFFSET:", OFFSET)

PAUSE = False
WINDOW = 500
fig, ((moving_average, spectrum_price), (bbp, spectrum_bbp)) = plt.subplots(2, 2, layout='constrained')

def animate(i):
    data_y = [ r[1] for r in OHLC_900[OFFSET+i:OFFSET+i+WINDOW] ]
    data_x = [ x for x in range(0, len(data_y)) ]
    
    MAs = [ data[OFFSET+i:OFFSET+i+WINDOW] for data in MA_SCALES]
    MA_BBPs = [ data[OFFSET+i:OFFSET+i+WINDOW] for data in MA_BBP_SCALES]

    SIGNAL = [ -1 + ((v / max(data_y)) * 2) for v in data_y ]
    
    POWER_SPECTRUM = power_spectrum_from_dft(SIGNAL)
    POWER_SPECTRUM_MA_16 = power_spectrum_from_dft(MAs[0])
    POWER_SPECTRUM_MA_32 = power_spectrum_from_dft(MAs[1])
                
    SIGNAL_BBP_1 = [ -1 + ((v / max(MA_BBPs[0])) * 2) for v in MA_BBPs[0] ]
    SIGNAL_BBP_2 = [ -1 + ((v / max(MA_BBPs[1])) * 2) for v in MA_BBPs[1] ]

    
    BBP_POWER_SPECTRUM_1 = power_spectrum_from_dft(SIGNAL_BBP_1)
    BBP_POWER_SPECTRUM_2 = power_spectrum_from_dft(SIGNAL_BBP_2)

    # plot
    moving_average.clear()
    bbp.clear()
    spectrum_bbp.clear()
    spectrum_price.clear()
    
    moving_average.set(
      xlim=(0, len(data_y)),
      ylim=(min(data_y), max(data_y))
    )

    bbp.set(
      xlim=(0, len(MA_BBPs[0])),
      ylim=(min(MA_BBPs[0]), max(MA_BBPs[0]))
    )
        
    bbp.grid(True, which='both')

    bbp.axhline(y=0, color='k')


    SPECTRUM_OFFSET = 0
    SPECTRUM_LIMIT = len(POWER_SPECTRUM)
       
    spectrum_price.semilogy([ i for i in range(SPECTRUM_OFFSET,SPECTRUM_LIMIT)], POWER_SPECTRUM[SPECTRUM_OFFSET:SPECTRUM_LIMIT], color=COLORS[0].hex)
    spectrum_price.semilogy([ i for i in range(SPECTRUM_OFFSET,SPECTRUM_LIMIT)], POWER_SPECTRUM_MA_16[SPECTRUM_OFFSET:SPECTRUM_LIMIT], color=COLORS[1].hex)
    spectrum_price.semilogy([ i for i in range(SPECTRUM_OFFSET,SPECTRUM_LIMIT)], POWER_SPECTRUM_MA_32[SPECTRUM_OFFSET:SPECTRUM_LIMIT], color=COLORS[2].hex)
    
    spectrum_bbp.semilogy([ i for i in range(0,len(BBP_POWER_SPECTRUM_1))], BBP_POWER_SPECTRUM_1, color=COLORS[1].hex)
    spectrum_bbp.semilogy([ i for i in range(0,len(BBP_POWER_SPECTRUM_2))], BBP_POWER_SPECTRUM_2, color=COLORS[2].hex)

    moving_average.plot(data_x, data_y, linewidth=LINE_WEIGHT, color=COLORS[0].hex)
    for i in range(0, len(MAs)):
      moving_average.plot([ v -(i) for v in data_x], MAs[i],    linewidth=LINE_WEIGHT, color=COLORS[i+1].hex)

    for i in range(0, len(MA_BBPs)):
        bbp.plot([ v -(i) for v in data_x], MA_BBPs[i],    linewidth=LINE_WEIGHT, color=COLORS[i+1].hex)
                  
ani = animation.FuncAnimation(fig , animate, interval=50)

plt.show()
