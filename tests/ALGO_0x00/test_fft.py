#! /usr/bin/env python3 

import numpy as np
import matplotlib.pyplot as plt

from dsociepy.finance.fourier_analysis import power_spectrum_from_dft

# Exemple de signal
fs = 500  # Fréquence d'échantillonnage en Hz
dt = 1/500
t = np.arange(0, 1, 1/fs)  # Échantillonnage sur 1 seconde
signal = np.sin(2 * np.pi * 5 * t) + np.sin(2 * np.pi * 20 * t) * 0.5
noise  = signal + 1*np.random.randn(len(signal))

fig, ((signal_plot, noise_plot, spectrum)) = plt.subplots(3, 1, layout='constrained')

signal_plot.plot(t,signal,color='blue', label='Clean')
noise_plot.plot(t,noise,color='red', label='noise')
power_spectrum = power_spectrum_from_dft(noise)
spectrum.plot([t for t in range(0, len(power_spectrum))], power_spectrum,color='blue', label='spectrum')

plt.show()
