#! /usr/bin/env python3

import os
import json

from statistics import mean

from dsociepy.finance.bear_bull_power import synth_bbp_from_trades

def compute_MA(data, length):
    return [ mean(data[i-length if i - length > 0 else 0:i+1]) for i in range(0, len(data)) ]

def prepare_ma():    
    PATH_ADAEUR = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/ohlcv-900.json")
    OHLC_900 = json.load(open(PATH_ADAEUR, 'r'))
    data_y = [ v[1] for v in OHLC_900 ]
    for length in range(2,34,2):
        print("Prepare prices Moving Average with length: ", length)
        ma = compute_MA(data_y, length)
        f = open("MA-"+str(length)+".json", "w")
        f.write(json.dumps(ma))
        f.close()

def prepare_BBP():
    print("Prepare BBP...")
    PATH_ADAEUR_TRADES = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/trades.json")
    trades = json.load(open(PATH_ADAEUR_TRADES, 'r'))
    BBP_900 = synth_bbp_from_trades(trades, 900)
        
    f = open("BBP_900.json", "w")
    f.write(json.dumps(BBP_900))
    f.close()
    
    for length in range(2,34,2):
        print("Prepare BBP Moving average with length: ", length)
        ma = compute_MA([v[1] for v in BBP_900], length)
        f = open("MA-BBP-"+str(length)+".json", "w")
        f.write(json.dumps(ma))
        f.close()

# ~ prepare_ma()
prepare_BBP()
