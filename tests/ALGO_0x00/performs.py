#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import os

OFFSET = 39797

portfolio = {
  "EUR": 150,
  "ADAEUR":0
}

TRADES_COUNT = 0
WRONG_TRADES = 0

SELLS = [-1] * OFFSET
BUYS  = [-1] * OFFSET

PATH_ADAEUR = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/ohlcv-900.json")

NOT_BEFORE = datetime.datetime(2020, 1, 10, 0, 0, 0, 0).timestamp()

OHLC_900 = json.load(open(PATH_ADAEUR, 'r'))
MA_SCALE_8 = json.load(open("MA-8.json", 'r'))
MA_SCALE_32 = json.load(open("MA-32.json", 'r'))

BBP_900 = json.load(open("BBP_900.json", 'r'))
MA_BBP_SCALES_8 = json.load(open("MA-BBP-8.json", 'r'))
MA_BBP_SCALES_32 = json.load(open("MA-BBP-32.json", 'r'))

print(len(BBP_900))
print(len(MA_BBP_SCALES_8))
print(len(MA_BBP_SCALES_32))
print(len(MA_SCALE_32))
print(len(MA_SCALE_8))
print(len(OHLC_900))
