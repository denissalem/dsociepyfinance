#! /usr/bin/env python3

import os
import json

from statistics import mean

from dsociepy.finance.bear_bull_power import synth_bbp_from_trades

PATH_ADAEUR = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/ohlcv-60.json")

def compute_MA(data, length):
    return [ mean(data[i-length if i - length > 0 else 0:i+1]) for i in range(0, len(data)) ]

def prepare_ma():    
    OHLC_60 = json.load(open(PATH_ADAEUR, 'r'))
    data_y = [ v[1] for v in OHLC_60 ]
    for length in [64, 256, 512]:
        print("Prepare prices Moving Average with length: ", length)
        ma = compute_MA(data_y, length)
        f = open("MA-"+str(length)+".json", "w")
        f.write(json.dumps(ma))
        f.close()

def prepare_BBP():
    print("Prepare BBP...")
    trades = json.load(open(PATH_ADAEUR_TRADES, 'r'))
    BBP_60 = synth_bbp_from_trades(trades, 60)
        
    f = open("BBP_60.json", "w")
    f.write(json.dumps(BBP_60))
    f.close()
    
    for length in [64, 256, 512]:
        print("Prepare BBP Moving average with length: ", length)
        ma = compute_MA([v[1] for v in BBP_60], length)
        f = open("MA-BBP-"+str(length)+".json", "w")
        f.write(json.dumps(ma))
        f.close()

def prepare_good_known_signal():
    print("Compute ideal signals...")
    OHLC_60 = json.load(open(PATH_ADAEUR, 'r'))
    ma_64  = json.load(open("MA-64.json", "r"))
    ma_256 = json.load(open("MA-256.json", "r"))
    signals = [{"action": None, 'price': (ma_64[i] + OHLC_60[i][1])/2} for i in range(0, len(ma_64))]
    
    last_reversal = 0
    for i in range(65, len(signals)):
        prices_window = [v["price"] for v in signals[last_reversal:i+1]]
        if  ma_64[i] > ma_256[i] and ma_64[i-1] < ma_256[i-1]:
            signal_index = last_reversal + prices_window.index( min(prices_window) )
            signals[signal_index]["action"] = 'b'
            print(signals[signal_index]["price"],last_reversal, signal_index, i)
            last_reversal = i

        elif ma_64[i] < ma_256[i] and ma_64[i-1] > ma_256[i-1]:
            signal_index = last_reversal + prices_window.index( max(prices_window) )
            signals[signal_index]["action"] = 's'
            print(signals[signal_index]["price"],last_reversal, signal_index, i)
            last_reversal = i


    open("signals.json", "w").write(json.dumps(signals))

# ~ prepare_ma()
# ~ prepare_BBP()

prepare_good_known_signal()
