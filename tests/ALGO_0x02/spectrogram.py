#! /usr/bin/env python3

import os
import json
import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import random
from statistics import mean
from datetime import timedelta

from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from matplotlib.pyplot import savefig

from dsociepy.finance.helpers import print_history

PATH_ADAEUR = os.path.expanduser("~/git/dsociepyfinance/tests/ADAEUR/ohlcv-60.json") # One minute OHLC
OHLC_60 = [ v[:5] for v in json.load(open(PATH_ADAEUR, 'r'))]
SIGNALS = json.load(open("signals.json",'r'))

MA_SCALES = [ json.load(open("MA-"+str(l)+".json", 'r')) for l in [64,256]]

OFFSET = 60*24*30
SPEED = 60

dt = 60
fs = 1/dt  # Fréquence d'échantillonnage en Hz
duration = 60*60*24
t = np.arange(0, duration, dt)  # Échantillonnage sur 1 seconde
WINDOW = len(t)
NOW = 86400


fig, ((ax1), (ax2)) = plt.subplots(2, 1, sharex=True, layout='constrained')

fig2, ((ax3), (ax4)) = plt.subplots(2, 1, sharex=True, layout='constrained')
  
nfft = 60

prices_phase_breaks_t = list()
prices_phase_breaks = list()

phase_signals = list()

local_signals = list()
local_signals_c = list()
local_signals_t = list()

BUYER = True

PORTE_FOLIO = {
    "CRYPTOSHITCOIN": 0,
    "EURO" : 200,
    "LAST_EURO": 200,
    "SAVED": 0
}

TRADE_HISTORY = []
TIME_ELAPSED = 0

PERFS_T = []
PERFS_PORTEFOLIO = []
PERFS_MARKET = []
PERFS_EUROS = []
PERFS_SAVED = []
MAX_TRADE_LIMIT = 500

def phases_normalisation(phases):
    low  = min([ min(v) for v in phases])
    high = max([ max(v) for v in phases])
    
    normalized_phases = [
        [ (phases[y][x] - low) / (high -  low) for x in range(0, len(phases[y])) ] for y in range(0, len(phases))
    ]
        
    return normalized_phases    
    
def update_phase_breaks(t, signals, phases, price, len_t):
    diffs = []
    for index in range(0,len(phases)):
        a = phases[index][-1]
        b = phases[index][-2]
        diffs.append( a - b  if a > b else b - a )
        
    for i in range(0, len(t)):
        t[i] = t[i] - (len_t*60)//24
    
    if max(diffs) >= 0.25:
        t.append((len_t-1)*60)
        signals.append(price)

    while len(t) and t[0] < 0:
        t.pop(0)
        signals.pop(0)
    
def period_trend(short_period, long_period):
  uptrend = 0
  for i in range(0, len(short_period)):
      uptrend += (short_period[i] - long_period[i]) * (i/len(short_period))
  
  return True if uptrend > 0 else False

def buy(STOCK_PRICES, index = -1, stop_loss=None):
    global BUYER
    global PORTE_FOLIO
    
    price = stop_loss if stop_loss != None else STOCK_PRICES["open"][index]
    
    local_signals_t.append(t[index])
    local_signals_c.append("green")
    local_signals.append(price)
    BUYER ^= True
        
    PORTE_FOLIO["CRYPTOSHITCOIN"] = PORTE_FOLIO["EURO"] / price
        
    TRADE_HISTORY.append({
        "action": "Buy ",
        "portefolio_value": PORTE_FOLIO["EURO"],
        "price" : price,
        "trade_rate" : ((TRADE_HISTORY[-1]["price"] / price)-1)*100 if len(TRADE_HISTORY) else 0
    })
            
    PORTE_FOLIO["EURO"] = 0
    print_history(TRADE_HISTORY)
    print("Trade at:", price)
    print("TIME ELAPSED:", timedelta(seconds=TIME_ELAPSED*60))

    
def sell(STOCK_PRICES, index = -1, stop_loss=None):
    global BUYER
    global PORTE_FOLIO
    
    price = stop_loss if stop_loss != None else STOCK_PRICES["open"][index]

    local_signals_t.append(t[index])
    local_signals_c.append("red")
    local_signals.append(price)
    BUYER ^= True
    PORTE_FOLIO["EURO"] = PORTE_FOLIO["CRYPTOSHITCOIN"] * price
    PORTE_FOLIO["LAST_EURO"] = PORTE_FOLIO["EURO"]

    limit_reached = PORTE_FOLIO["EURO"] >= MAX_TRADE_LIMIT
    
    if limit_reached:
        PORTE_FOLIO["SAVED"] += PORTE_FOLIO["EURO"] - MAX_TRADE_LIMIT
        PORTE_FOLIO["EURO"] = MAX_TRADE_LIMIT

    TRADE_HISTORY.append({
        "action": "Sell",
        "portefolio_value": PORTE_FOLIO["EURO"],
        "price" : price,
        "trade_rate" : ((price / TRADE_HISTORY[-1]["price"])-1)*100 if len(TRADE_HISTORY) else 0
    })
    PORTE_FOLIO["CRYPTOSHITCOIN"] = 0
    print_history(TRADE_HISTORY)
    print("Trade at:", price)
    print("TIME ELAPSED:", timedelta(seconds=TIME_ELAPSED*60))

def build_matchs(prices_t, prices, best_moves, stop_losses):
    matchs = {}
    for i in range(0, len(prices)):
        matchs[prices_t[i]] = prices[i]
        
    for i in range(0, len(best_moves)):
        if best_moves[i]["action"] in ['s','b']:
            matchs[i*60] = best_moves[i]["price"]
        
    return sorted(matchs.keys()), [matchs[key] for key in sorted(matchs.keys())]
        
    
def trigger_phase_inversion(matchs):
    if BUYER:
        if matchs[-3] < matchs[-2] and matchs[-2] < matchs[-1]:
            return True
            
        return False
    else:
        if matchs[-3] > matchs[-2] and matchs[-2] > matchs[-1]:
            return True
            
        return False
        
def get_last_best_move(best_moves):
    pair = {
        "index" : None,
        "action": None
    }
    
    for i in range(0, len(best_moves)):
        if best_moves[i]["action"] in ["s","b"]:
            pair["index"] = i
            pair["action"] = best_moves[i]["action"]
            
    return pair

def animate(frame_index):
    global TIME_ELAPSED
    TIME_ELAPSED += 60
    ax1.clear()
    ax2.clear()
    i = frame_index
    STOCK_PRICES = {
      "open":  [ v[1] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ],
      "high":  [ v[2] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ],
      "low":   [ v[3] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ],
      "close": [ v[4] for v in OHLC_60[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    }

    if not len(STOCK_PRICES["open"]):
        print("END_OF_SIMULATON")
        input()
    
    SIGNALS_WINDOW = SIGNALS[OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW]
    
    #####################
    # PROCESS STOP LOSS #
    #####################
    
    stop_losses = []
    if len(TRADE_HISTORY):
        for t_index in range(len(t) - 60, len(t)):
            if TRADE_HISTORY[-1]["action"] == "Buy" and STOCK_PRICES["open"][t_index] < TRADE_HISTORY[-1]["price"] / 1.015:
                print("DEBUG:",TRADE_HISTORY[-1]["action"], STOCK_PRICES["open"][t_index], TRADE_HISTORY[-1]["price"] / 1.05, TRADE_HISTORY[-1]["price"] * 1.015 )
                sell(STOCK_PRICES, index=t_index, stop_loss=TRADE_HISTORY[-1]["price"] / 1.015)
                print("DEBUG: STOP LOSS AND SELL")
                break
                 
            elif TRADE_HISTORY[-1]["action"] == "Sell" and STOCK_PRICES["open"][t_index] > TRADE_HISTORY[-1]["price"] * 1.015:
                print("DEBUG", TRADE_HISTORY[-1]["action"], STOCK_PRICES["open"][t_index], TRADE_HISTORY[-1]["price"] * 1.015, TRADE_HISTORY[-1]["price"] * 1.015 )
                buy(STOCK_PRICES, index=t_index, stop_loss=TRADE_HISTORY[-1]["price"] * 1.015)
                print("DEBUG: STOP LOSS AND BUY")
                break
                
    #########################
    # END PROCESS STOP LOSS #
    #########################
    
    MA_256 =  [ v for v in MA_SCALES[1][OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    MA_64 =   [ v for v in MA_SCALES[0][OFFSET+i*SPEED:OFFSET+i*SPEED+WINDOW] ]
    
    ax1.set(
      xlim=(0, len(t)),
      ylim=(min(STOCK_PRICES["open"]), max(STOCK_PRICES["open"]))
    )
    
    ax1.plot(t, STOCK_PRICES["open"], color='black', linewidth = 0.5)    
    ax1.plot(t, MA_64, linewidth = 0.5)
    ax1.plot(t, MA_256, linewidth = 0.5)

    ax1.grid(False, which='both')
    for loc in range(0, len(t)*60, (len(t)*60)//24):
        ax1.axvline(loc, alpha=0.2, color='#b0b0b0', linestyle='-', linewidth=0.8)
    
    Pxx, freqs, bins, im = ax2.specgram(STOCK_PRICES["open"], NFFT=nfft, Fs=fs, noverlap=0, mode='phase')
    
    update_phase_breaks(
        prices_phase_breaks_t,
        prices_phase_breaks,
        phases_normalisation(Pxx),
        STOCK_PRICES["open"][-1],
        len(t)
    )   
        
    matchs_t, matchs = build_matchs(
        prices_phase_breaks_t,
        prices_phase_breaks,
        SIGNALS_WINDOW,
        stop_losses
        
    )
    
    #####################
    # SIGNAL PROCESSING #
    #####################
        
    for i in range(0, len(local_signals_t)):
        local_signals_t[i] = local_signals_t[i] - (len(t)*60)//24

    while len(local_signals_t) and local_signals_t[0] < 0:
        local_signals_t.pop(0)
        local_signals_c.pop(0)
        local_signals.pop(0)
            
    if len(matchs_t) and matchs_t[-1] == t[-1]:
        uptrend = period_trend(MA_64[-(len(t)//24):], MA_256[-(len(t)//24):])
        overbuy = period_trend(STOCK_PRICES["open"][-(len(t)//24):], MA_64[-(len(t)//24):])
                    
        global BUYER

        last_know_best_move = get_last_best_move(SIGNALS_WINDOW)

        if BUYER:
            # On se positionne :
            # - si downtrend et oversell
            # - si on peut prendre de la marge
            # ~ if ((not uptrend) and (not overbuy)) or (len(TRADE_HISTORY) and TRADE_HISTORY[-1]["price"] > STOCK_PRICES["open"][-1]):
            if ((not uptrend) and (not overbuy)) or (len(TRADE_HISTORY) and TRADE_HISTORY[-1]["price"] > STOCK_PRICES["open"][-1]):
                if not (len(STOCK_PRICES["open"]) - last_know_best_move["index"] < (len(t)//24) // 3 and last_know_best_move["action"] == "s"):
                    buy(STOCK_PRICES)
            
            # ~ elif trigger_phase_inversion(matchs):
                # ~ if not (len(STOCK_PRICES["open"]) - last_know_best_move["index"] < (len(t)//24) // 3 and last_know_best_move["action"] == "s"):
                    # ~ buy(STOCK_PRICES)
        
        else:
            # On se positionne :
            # - si uptrend et overbuy
            # - si on peut prendre de la marge
            if (uptrend and overbuy) or (len(TRADE_HISTORY) and TRADE_HISTORY[-1]["price"] < STOCK_PRICES["open"][-1]):
                if not (len(STOCK_PRICES["open"]) - last_know_best_move["index"] < (len(t)//24) // 3 and last_know_best_move["action"] == "b"):
                    sell(STOCK_PRICES)

            # ~ elif trigger_phase_inversion(matchs):
                # ~ if not (len(STOCK_PRICES["open"]) - last_know_best_move["index"] < (len(t)//24) // 3 and last_know_best_move["action"] == "b"):
                    # ~ sell(STOCK_PRICES)
                
        PERFS_T.append(TIME_ELAPSED)
        PERFS_PORTEFOLIO.append(
            PORTE_FOLIO["EURO"] + PORTE_FOLIO["CRYPTOSHITCOIN"] * STOCK_PRICES["open"][-1]
        )
        PERFS_MARKET.append(STOCK_PRICES["open"][-1])
        PERFS_EUROS.append(PORTE_FOLIO["LAST_EURO"])
        PERFS_SAVED.append(PORTE_FOLIO["SAVED"])
    
    #####################
    #      END OF       #
    # SIGNAL PROCESSING #
    #####################
    
    if len(PERFS_T):
        ax3.clear()
        ax4.clear()
        ax3.plot(PERFS_T, PERFS_PORTEFOLIO, color='grey', linewidth = 0.5)    
        ax3.plot(PERFS_T, PERFS_EUROS, color='black', linewidth = 0.5)    
        ax3.plot(PERFS_T, PERFS_SAVED, color='gold', linewidth = 0.5)    
        ax4.plot(PERFS_T, PERFS_MARKET, color='black', linewidth = 0.5)    

    if len(prices_phase_breaks):
        ax1.scatter(
            matchs_t,
            matchs,
            marker='o',
            c='k',
            s=80
        )
        
    if len(local_signals_t):
        ax1.scatter(
            local_signals_t,
            local_signals,
            marker='o',
            c=local_signals_c,
            s=80
        )
        
    ax1.scatter(
        t,
        [s["price"] for s in SIGNALS_WINDOW],
        marker='o',
        c=['red' if s["action"] == 's' else 'green' for s in SIGNALS_WINDOW],
        s=[0 if not s["action"] in ['s','b'] else 20 for s in SIGNALS_WINDOW],
    )
    
    savefig("frames/{:04x}.png".format(frame_index), transparent=False, dpi='figure', format="png",
      metadata=None, bbox_inches=None, pad_inches=0.1,
      facecolor='auto', edgecolor='auto', backend=None
    )
        
ani = animation.FuncAnimation(fig , animate, interval=50)

plt.show()
