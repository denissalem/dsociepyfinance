#! /usr/bin/env python3

#    Copyright 2019, 2023 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from dsociepy.finance import Backtest, Position
from dsociepy.finance.ohlcv import synth_ohlcv

import make_additional_data as extra_data

import time

fake_data_simple_case = [
    #<close> <Gain> <Loss>  <AG>    <AL>    <RSI>
    [44.34, None,   None,   None,   None,   None],  
    [44.09, 0.00,   0.25,   None,   None,   None],              
    [44.15, 0.06,   0.00,   None,   None,   None],              
    [43.61, 0.00,   0.54,   None,   None,   None],              
    [44.33, 0.72,   0.00,   None,   None,   None],              
    [44.83, 0.50,   0.00,   None,   None,   None],              
    [45.10, 0.27,   0.00,   None,   None,   None],              
    [45.42, 0.33,   0.00,   None,   None,   None],              
    [45.84, 0.42,   0.00,   None,   None,   None],              
    [46.08, 0.24,   0.00,   None,   None,   None],              
    [45.89, 0.00,   0.19,   None,   None,   None],              
    [46.03, 0.14,   0.00,   None,   None,   None],              
    [45.61, 0.00,   0.42,   None,   None,   None],      
    [46.28, 0.67,   0.00,   None,   None,   None],
    [46.28, 0.00,   0.00,   0.24,   0.10,   70.53],
    [46.00, 0.00,   0.28,   0.22,   0.11,   66.32],
    [46.03, 0.03,   0.00,   0.21,   0.10,   66.55],
    [46.41, 0.38,   0.00,   0.22,   0.10,   69.41],
    [46.22, 0.00,   0.19,   0.20,   0.10,   66.36],
    [45.64, 0.00,   0.58,   0.19,   0.14,   57.97],
    [46.21, 0.57,   0.00,   0.22,   0.13,   62.93],
    [46.25, 0.04,   0.00,   0.20,   0.12,   63.26],
    [45.71, 0.00,   0.54,   0.19,   0.15,   56.06],
    [46.45, 0.74,   0.00,   0.23,   0.14,   62.38],
    [45.78, 0.00,   0.67,   0.21,   0.18,   54.71],
    [45.35, 0.00,   0.43,   0.20,   0.19,   50.42],
    [44.03, 0.00,   1.33,   0.18,   0.27,   39.99],
    [44.18, 0.15,   0.00,   0.18,   0.26,   41.46],
    [44.22, 0.04,   0.00,   0.17,   0.24,   41.87],
    [44.57, 0.35,   0.00,   0.18,   0.22,   45.46],
    [43.42, 0.00,   1.15,   0.17,   0.29,   37.30],
    [42.66, 0.00,   0.76,   0.16,   0.32,   33.08],
    [43.13, 0.47,   0.00,   0.18,   0.30,   37.77]
]

class TestBacktest(Backtest):
    def __init__(self, asset_pair, ut, money=100, flags=31, loss_limit=50):
        self.tested_rsi = {}
        self.ut = ut
        super().__init__(asset_pair, money, flags, loss_limit)

    def _yield_new_data(self):
        for i in range(0, len(extra_data.trades_limited)):
            yield extra_data.trades_limited[i][:3]+['b']

        return None

    def algo(self):
        self.tested_rsi[self._rsi[14][self.ut][-1][0]] = self._rsi[14][self.ut][-1]
    
    def rsi_computation_simple_case(self):
        self.add_rsi(14)
        self.add_ohlcv(self.ut)
        for v in fake_data_simple_case:
            self._ohlcv[self.ut].append([time.time()]+([v[0]]*4)+[1] )
            self._compute_rsi()
            rsi = [None]
            rsi.append(self.get_current_rsi_gain(14,self.ut))
            rsi.append(self.get_current_rsi_loss(14,self.ut))
            rsi.append(self.get_current_rsi_average_gain(14,self.ut))
            rsi.append(self.get_current_rsi_average_loss(14,self.ut))
            rsi.append(self.get_current_rsi(14,self.ut))
            for i in range(1,6):
                try:
                    if v[i] == None and rsi[i] != None:
                        raise Exception
                    
                    elif v[i] != None and (abs(round(v[i] - rsi[i],2)) > 0.1):
                        raise Exception
                        
                except:
                    raise Exception(v[i], rsi[i], v, rsi)
        
        return True
                    

bt = TestBacktest("ADAEUR", 3600)

def rsi_computation_with_atom_trades(ut):
    bt = TestBacktest("ADAEUR", ut)
    bt.add_ohlcv(ut)
    bt.add_rsi(14)
    bt.run()
    keys = [key for key in sorted(bt.tested_rsi.keys())]
    for i in range(0, len(keys)):
        tested = [ v if v != None else 0 for v in bt.tested_rsi[keys[i]]]
        ref = [ v if v != None else 0 for v in extra_data.rsi[ut][i]]
        #print('> TESTED', i, tested[1:])
        #print('> REF   ', i, ref[1:])
        #print()
        if tested[1:] != ref[1:]:
            return False
        
    return True

print("Testing RSI Computation, simple case...")
assert(bt.rsi_computation_simple_case())
print("Testing RSI Computation with atom trades...")
print("\t",300,"...")
assert(rsi_computation_with_atom_trades(300))
print("\t",900,"...")
assert(rsi_computation_with_atom_trades(900))
print("Test: RSI Computation: Pass")
