#!/usr/bin/env python3

#    Copyright 2019, 2024 Denis Salem
#
#    This file is part of DSOCIEPY.
#
#    DSOCIEPY is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    DSOCIEPY is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with DSOCIEPY.  If not, see <http://www.gnu.org/licenses/>.

from setuptools import setup
import os

setup(name="dsociepy.finance",
    version="1.0.0",
    description="FOSS compliant finance tools from DSOCIETY",
    author="Denis Salem",
    author_email="dsociepy@tuxfamily.org",
    url='https://framagit.org/DenisSalem/dsociepyfinance',
    packages=[
        'dsociepy',
        'dsociepy.finance',
        'dsociepy.finance.market_interfaces',
        'dsociepy.finance.plot',
    ],
    license="GNU/GPLv3",
    platforms="Linux",
    long_description="FOSS compliant python tools from DSOCIETY",
    classifiers=[
        "Environment :: Console",
        "Development Status :: 5 - Production/Stable"
    ],
    install_requires=[
        'pycurl',
    ],
    scripts=[
        "dsociepy.finance.market_recorder",
        "dsociepy.finance.phasebreaks_algotrader"
    ],
    data_files = []
)
