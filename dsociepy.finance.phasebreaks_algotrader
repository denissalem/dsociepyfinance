#! /usr/bin/env python3

import datetime
import os
import json
import random
import time

from collections import deque
from collections import namedtuple
from statistics import mean

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.animation as animation
from matplotlib.pyplot import savefig
import matplotlib.dates as mdates

import pycurl

from dsociepy.finance.rsi import compute_rsi
from dsociepy.finance.rsi import compute_local_rsi
from dsociepy.finance.helpers import print_history
from dsociepy.finance.market_interfaces.kraken import Kraken
from dsociepy.finance.ohlcv import synth_ohlcv_from_trades

FIG_COUNT = 0
CHUNK = 60 # minutes
WINDOW = 24 * CHUNK
SPEED = 60
MAX_TRADE_LIMIT = 2000
FOUR_WEEKS = 4*7*24*60*60

DT = 60
FS = 1/DT  # Fréquence d'échantillonnage en Hz

REFRESH_DELTA = 0

Trade = namedtuple(
  "Trade",
  "timestamp action portefolio_value saved price trade_rate stop_loss"
)

StopLossTrigger = namedtuple(
  "StopLossTrigger",
  "timestamp action price"
)

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!! TODO : When no transaction are made, acquire spread to set actual price
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def log_internal(identifier, message):
    time_identifier = datetime.datetime.now().strftime("%Y-%m-%d")
    path = os.path.expanduser("~/dsociepy.finance.phasebreaks_algotrader-"+time_identifier+".log")
    line = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")+"\t"+identifier+"\t"+message+"\n"
    if not os.path.isfile(path):
        with open(path, "w") as f:
            f.write(line)

    else:
        f = open(path, "r")
        content = f.read() + line
        f.close()
        
        with open(path, "w") as f:
            f.write(content)
            

def period_trend(short_period, long_period):
  uptrend = 0
  for i in range(0, len(short_period)):
      uptrend += (short_period[i] - long_period[i]) * (i/len(short_period))
  
  return True if uptrend > 0 else False

class Performances:
    def __init__(self, ):
        self.timestamps = []
        self.portefolio = []
        self.market = []
        self.euros = []
        self.saved = []

class Strategy:
    def __init__(self, portefolio_value):
        self.initial_value = portefolio_value
        self.assets = 0
        self.euros = portefolio_value
        self.last_euros = portefolio_value
        self.saved = 0
        self.trades_history = []
        self.phase_breaks_prices = []
        self.phase_breaks_t = []
        self.schedule = None
        self.state_is_buyer = True
        self.trades_history = []
        self.performances = Performances()
        self.stop_loss_triggers = []
        self.stop_loss_factor = 1.015
    
    def brake(self, stream):
        down_area = 0
        up_area = 0
        
        for index in range(0, len(stream.window_prices_open)):
            up_area += stream.window_prices_open[index] - stream.brake_ma[index] if stream.window_prices_open[index] > stream.brake_ma[index] else 0
            down_area += stream.brake_ma[index] - stream.window_prices_open[index] if stream.window_prices_open[index] < stream.brake_ma[index] else 0
            
        return True if down_area > up_area else False
    
    def update_schedule(self, timestamp):
        if self.schedule == None:
            self.schedule = timestamp
            return False
            
        elif timestamp - self.schedule >= 3600:
            self.schedule = timestamp
            return True
            
        else:
            return False
            
    def phases_normalisation(self, phases):
        low  = min([ min(v) for v in phases])
        high = max([ max(v) for v in phases])
        
        normalized_phases = [
            [ (phases[y][x] - low) / (high -  low) for x in range(0, len(phases[y])) ] for y in range(0, len(phases))
        ]
            
        return normalized_phases    
    
    def update_phasebreaks(self, pxx, stream):
        diffs = []
        phases = self.phases_normalisation(pxx)
        
        for index in range(0,len(phases)):
            a = phases[index][-1]
            b = phases[index][-2]
            diffs.append( a - b  if a > b else b - a )
            
        if max(diffs) >= 0.25:
            self.phase_breaks_t.append(stream.window_t[-1])
            self.phase_breaks_prices.append(stream.window_prices_open[-1])
    
        while len(self.phase_breaks_t) and  self.phase_breaks_t[0] < stream.window_t[0]:
            self.phase_breaks_t.pop(0)
            self.phase_breaks_prices.pop(0)
            
    def perform(self, stream):
        log_internal(
            "PERFORM",
            str(len(self.phase_breaks_t))+"\t"+
            (str(self.phase_breaks_t[-1] == stream.window_t[-1]) if len(self.phase_breaks_t) else "NaN")+"\t"+
            str(not self.brake(stream))+"\t"+
            str(len(self.phase_breaks_t) and self.phase_breaks_t[-1] == stream.window_t[-1] and not self.brake(stream))
        )
        if len(self.phase_breaks_t) and self.phase_breaks_t[-1] == stream.window_t[-1] and not self.brake(stream):
            # uglyyyyy111
            # TODO : https://stackoverflow.com/questions/7064289/use-slice-notation-with-collections-deque
            uptrend = period_trend(list(stream.short_ma)[-60:], list(stream.long_ma)[-60:])
            overbuy = period_trend(list(stream.window_prices_open)[-60:], list(stream.short_ma)[-60:])
            
            last_know_best_move = stream.get_last_best_move()
    
            # TODO do not perform contradictory action with recent stop loss
            if self.state_is_buyer:
                log_internal("IS_BUYER","")
                # On se positionne :
                # - si downtrend et oversell
                # - si on peut prendre de la marge
                log_internal(
                    "SET_BUY_ORDER_STEP_1",
                    str((not uptrend))+"\t"+
                    str((not overbuy))+"\t"+
                    str(len(self.trades_history))+"\t"+
                    str(self.trades_history[-1].price if len(self.trades_history) else "Nan")+"\t"+
                    str(self.stop_loss_factor)+"\t"+
                    str(stream.window_prices_open[-1])+"\t"+
                    str(((not uptrend) and (not overbuy)) or (len(self.trades_history) and self.trades_history[-1].price / self.stop_loss_factor > stream.window_prices_open[-1] ))
                )
                if ((not uptrend) and (not overbuy)) or (len(self.trades_history) and self.trades_history[-1].price / self.stop_loss_factor > stream.window_prices_open[-1] ):
                    last_know_best_best_move_allow = not (stream.window_t[-1] - last_know_best_move["timestamp"] < 3600 // 3 and last_know_best_move["type"] == "red")
                    last_trade_allow = True if len(strategy.trades_history) == 0 else not (stream.window_t[-1] - strategy.trades_history[-1].timestamp < 3600 // 3 and strategy.trades_history[-1].action == "red")
                    log_internal(
                        "SET_BUY_ORDER_STEP_2",
                        str(last_know_best_best_move_allow)+"\t"+
                        str(last_trade_allow)
                    )
                    if last_know_best_best_move_allow and last_trade_allow:
                        self.buy(stream)
                        self.update_performances(stream)

            else:
                log_internal("IS_SELLER","")

                # On se positionne :
                # - si uptrend et overbuy
                # - si on peut prendre de la marge
                log_internal(
                    "SET_SELL_ORDER_STEP_1",
                    str((uptrend))+"\t"+
                    str((overbuy))+"\t"+
                    str(len(self.trades_history))+"\t"+
                    str(self.trades_history[-1].price if len(self.trades_history) else "Nan")+"\t"+
                    str(self.stop_loss_factor)+"\t"+
                    str(stream.window_prices_open[-1])+"\t"+
                    str(((uptrend) and (overbuy)) or (len(self.trades_history) and self.trades_history[-1].price * self.stop_loss_factor < stream.window_prices_open[-1] ))
                )
                if (uptrend and overbuy ) or (len(self.trades_history) and self.trades_history[-1].price * self.stop_loss_factor < stream.window_prices_open[-1]):
                    last_know_best_best_move_allow = not ( stream.window_t[-1] - last_know_best_move["timestamp"] < 3600 // 3 and last_know_best_move["type"] == "green" )
                    last_trade_allow = True if len(strategy.trades_history) == 0 else not (stream.window_t[-1] - strategy.trades_history[-1].timestamp < 3600 // 3 and strategy.trades_history[-1].action == "green")
                    log_internal(
                        "SET_BUY_ORDER_STEP_2",
                        str(last_know_best_best_move_allow)+"\t"+
                        str(last_trade_allow)
                    )
                    if last_know_best_best_move_allow and last_trade_allow:
                        self.sell(stream)
                        self.update_performances(stream)
            
    def update_performances(self, stream, stop_loss=False):
        self.performances.timestamps.append(stream.window_t[-1])
        self.performances.portefolio.append(
            self.euros + self.assets * stream.window_prices_open[-1]
        )
        self.performances.market.append(stream.window_prices_open[-1])
        self.performances.euros.append(self.last_euros)
        self.performances.saved.append(self.saved)

    def buy(self, stream, stop_loss=None):        
        price = stream.window_prices_open[-1] if stop_loss == None else stop_loss
        
        trade = Trade(
            timestamp=stream.window_t[-1],
            action="green",
            portefolio_value=self.euros,
            saved=self.saved,
            price=price,
            trade_rate= ((self.trades_history[-1].price / price) -1)*100 if len(self.trades_history) else 0,
            stop_loss= True if stop_loss != None else False
        )
        log_internal("DO_BUY", str(trade))
        self.trades_history.append(trade)        
        
        self.assets = self.euros / price
        self.euros = 0
        self.state_is_buyer ^= True
        print_history(self.trades_history)
        if stop_loss == None:
            self.push_stop_loss(stream.window_t[-1], "red", price)
        else:
            log_internal("CLEAR_STOPLOSS", "")
            self.stop_loss_triggers.clear()

    def sell(self, stream, stop_loss=None):    
        price = stream.window_prices_open[-1] if stop_loss == None else stop_loss
    
        self.euros = self.assets * price
        self.last_euros = self.euros
    
        limit_reached = self.euros >= MAX_TRADE_LIMIT
        log_internal("SELL_LIMIT_REACHED", str(limit_reached))
        if limit_reached:
            self.saved += self.euros - MAX_TRADE_LIMIT
            self.euros = MAX_TRADE_LIMIT
        
        trade = Trade(
            timestamp=stream.window_t[-1],
            action="red",
            portefolio_value=self.euros,
            saved=self.saved,
            price=price,
            trade_rate=((price / self.trades_history[-1].price)-1)*100 if len(self.trades_history) else 0,
            stop_loss= True if stop_loss != None else False

        )
        
        log_internal("DO_SELL", str(trade))
        
        self.trades_history.append(trade)     
        
        self.assets = 0
        self.state_is_buyer ^= True
        print_history(self.trades_history)
        if stop_loss == None :
            self.push_stop_loss(stream.window_t[-1], "green", price)
        else:
            log_internal("CLEAR_STOPLOSS", "")
            self.stop_loss_triggers.clear()
            
    def push_stop_loss(self, timestamp, action, price):
        stoploss = StopLossTrigger(
            timestamp=timestamp,
            action=action,
            price=price * self.stop_loss_factor if action == "green" else price / self.stop_loss_factor
        )
        
        log_internal("PUSH_STOPLOSS", str(stoploss))
        self.stop_loss_triggers.append(stoploss)         

        
class StreamMarketData:
    def acquire_recent_data(self, asset_pair):
        last = 0
        trades = []
        while True:
            time.sleep(5)

            # TODO :
            # - Log messages
            
            try:
                data = None
                data = Kraken.recent_trades(asset_pair, self.api_since)
                last = data["result"]["last"]
                if len(data["result"][asset_pair]):
                    print("Acquire trade from: ", datetime.datetime.fromtimestamp(int(data["result"][asset_pair][0][2])), "to ", datetime.datetime.fromtimestamp(int(data["result"][asset_pair][-1][2])))
                    trades += data["result"][asset_pair]
                    
                else:
                    break
                    
                if last == self.api_since:
                    break
                    
                self.api_since = last
              
            except pycurl.error as e:
                print("PyCurl Error:", e)
                time.sleep(5)
                continue
                
            except Exception as e:
                print("Acquire trade Error:", e, data, last)
                time.sleep(5)
                continue
                        
        # acquire Spread
        while True:
            try:
                print("Acquire spreads ...")
                spread = Kraken.recent_spreads(asset_pair)["result"][asset_pair]
                # TODO Verify data integrity: errors, empty list ...
                break
                
            except Exception as e:
                print("Acquire spread Error:", e)
                time.sleep(5)
                continue
                
        return sorted(trades, key = lambda x : x[2]), spread
              
    def __init__(self, asset_pair, ut, short_ma, long_ma, brake_period):
        self.asset_pair = asset_pair
        
        self.ut = ut
        self.short_ma_length = short_ma
        self.long_ma_length = long_ma
        self.brake_ma_length = brake_period
        self.short_ma = None
        self.long_ma = None
        self.brake_ma = None
        self.window_prices_open = None
        self.window_t = None
        
        self.api_since = int(time.time()) - (48*3600)
        self.raw_trades = []

    # Doesn't provide signal, but help to prevent bad moves
    # TODO : NEED TO OPTIMIZED, IT'S UGLY !!
    def compute_best_reversal_move(self):
        last_reversal_index = 0
        
        signal_indexes = []
        signal_types = []
        for index in range(1, len(self.window_t)):
            if self.short_ma[index] > self.long_ma[index] and self.short_ma[index-1] < self.long_ma[index-1]:
                sub_window = tuple(self.window_prices_open)[last_reversal_index:index]
                signal_indexes.append(last_reversal_index + sub_window.index( min(sub_window) ))
                signal_types.append('green')
                last_reversal_index = index
    
            elif self.short_ma[index] < self.long_ma[index] and self.short_ma[index-1] > self.long_ma[index-1]:
                sub_window = tuple(self.window_prices_open)[last_reversal_index:index]
                signal_indexes.append(last_reversal_index + sub_window.index( max(sub_window) ))
                signal_types.append('red')
                last_reversal_index = index
            
        self.best_known_moves_price = [ self.window_prices_open[i] for i in signal_indexes ]
        self.best_known_moves_t = [ self.window_t[i] for i in signal_indexes ]
        self.best_known_moves_type = signal_types
        
    def get_last_best_move(self):
        if len(self.best_known_moves_t):
            return {
                "timestamp" : self.best_known_moves_t[-1],
                "price": self.best_known_moves_price[-1],
                "type": self.best_known_moves_type[-1]
            }
        else:
            return {
                "timestamp": 0,
                "price": None,
                "type": None
            }
          
    def initialize_MA(self, data, length):
        return [ mean(data[i-length if i - length > 0 else 0:i+1]) for i in range(0, len(data)) ], len(data)

    def update_data(self):
        trades, spread = self.acquire_recent_data(self.asset_pair)                
        self.raw_trades = [ trade for trade in self.raw_trades if trade[2] > int(time.time()) - (48*3600) ] + trades
        
        self.ohlc = synth_ohlcv_from_trades(self.raw_trades, spread)[self.ut]
        self.ohlc = [ [key]+self.ohlc[key] for key in sorted(self.ohlc.keys()) ]
        
        # TODO : https://stackoverflow.com/questions/10003143/how-to-slice-a-deque
        window = [ v[:4] for v in self.ohlc ]
                
        self.window_prices_open = [ v[1] for v in window ]
        self.window_prices_high = [ v[2] for v in window ]
        self.window_prices_low =  [ v[3] for v in window ]
        self.window_t = [ v[0] for v in window ]
        
        self.short_ma = self.initialize_MA(self.window_prices_open, self.short_ma_length)[0][-WINDOW:]
        self.long_ma = self.initialize_MA(self.window_prices_open, self.long_ma_length)[0][-WINDOW:]
        self.brake_ma = self.initialize_MA(self.window_prices_open, self.brake_ma_length)[0][-WINDOW:]
        
        self.window_t = self.window_t[-WINDOW:]
        self.window_prices_open = self.window_prices_open[-WINDOW:]
        self.window_prices_high = self.window_prices_high[-WINDOW:]
        self.window_prices_low = self.window_prices_low[-WINDOW:]
        
        self.compute_best_reversal_move()

def animate(frame_index):
    apply_strategy = False
    global REFRESH_DELTA
    if time.time() - REFRESH_DELTA > 30:
        stream.update_data()
        
        # ~ # Process Stop losses
        if len(strategy.stop_loss_triggers):
            if strategy.stop_loss_triggers[-1].action == "green" and stream.window_prices_high[-1] > strategy.stop_loss_triggers[-1].price:
                strategy.buy(stream, stop_loss=strategy.stop_loss_triggers[-1].price)
                strategy.update_performances(stream)
    
            elif strategy.stop_loss_triggers[-1].action == "red" and stream.window_prices_low[-1] < strategy.stop_loss_triggers[-1].price:
                strategy.sell(stream, stop_loss=strategy.stop_loss_triggers[-1].price)
                strategy.update_performances(stream)
    
            # make stop_loss follow
            elif strategy.stop_loss_triggers[-1].action == "green" and stream.window_prices_high[-1] * strategy.stop_loss_factor < strategy.stop_loss_triggers[-1].price:
                strategy.stop_loss_triggers[-1] = StopLossTrigger(
                    timestamp=strategy.stop_loss_triggers[-1].timestamp,
                    action=strategy.stop_loss_triggers[-1].action,
                    price=stream.window_prices_high[-1] * strategy.stop_loss_factor
                )
                
            elif strategy.stop_loss_triggers[-1].action == "red" and stream.window_prices_low[-1] / strategy.stop_loss_factor > strategy.stop_loss_triggers[-1].price:
                strategy.stop_loss_triggers[-1] = StopLossTrigger(
                    timestamp=strategy.stop_loss_triggers[-1].timestamp,
                    action=strategy.stop_loss_triggers[-1].action,
                    price=stream.window_prices_high[-1] / strategy.stop_loss_factor
                )
                            
        # ~ # Apply strategy
        apply_strategy = strategy.update_schedule(stream.window_t[-1])
        if apply_strategy:
            # PHASE SPECTROGRAM
            ax2.clear()
            Pxx, freqs, bins, im = ax2.specgram(list(stream.window_prices_open), NFFT=60, Fs=FS, noverlap=0, mode='phase')
            strategy.update_phasebreaks(Pxx, stream)
            strategy.perform(stream)
            
        REFRESH_DELTA = time.time()

    # PHASE SPECTROGRAM
    ax2.clear()
    Pxx, freqs, bins, im = ax2.specgram(list(stream.window_prices_open), NFFT=60, Fs=FS, noverlap=0, mode='phase')
    
    # ~ # PRICES
    ax1.clear()
    for loc in range(int(stream.window_t[0]), int(stream.window_t[-1]), (int(stream.window_t[-1]) - int(stream.window_t[0]))//24):
        ax1.axvline(datetime.datetime.fromtimestamp(loc), alpha=0.2, color='#808080', linestyle='-', linewidth=0.8)
    
    dates = [ datetime.datetime.fromtimestamp(int(t)) for t in stream.window_t ]
        
    ax1.set_xlim([dates[0], dates[-1]])
    ax1.set_ylim([min(stream.window_prices_open), max(stream.window_prices_open)])
    ax1.plot(dates, stream.window_prices_open, color='black', linewidth = 0.5)
    ax1.plot(dates, stream.short_ma, color='blue', linewidth = 0.5)
    ax1.plot(dates, stream.long_ma, color='violet', linewidth = 0.5)
    ax1.plot(dates, stream.brake_ma, color='red', linewidth = 0.5)
    
    ax1.scatter(
        [datetime.datetime.fromtimestamp(int(t)) for t in stream.best_known_moves_t],
        stream.best_known_moves_price,
        marker='x',
        c=stream.best_known_moves_type,
        s=50,
    )
    
    ax1.scatter(
        [datetime.datetime.fromtimestamp(int(t)) for t in strategy.phase_breaks_t],
        strategy.phase_breaks_prices,
        marker='o',
        c='black',
        s=50,
    )
    
    # TODO: cache index
    performed_strategy = [ trade for trade in strategy.trades_history if trade.timestamp >= stream.window_t[0] ]

    ax1.scatter(
        [datetime.datetime.fromtimestamp(trade.timestamp) for trade in performed_strategy],
        [trade.price for trade in performed_strategy],
        marker='o',
        c=[trade.action for trade in performed_strategy],
        s=25,
    )
    
    # ~ # PERFORMANCES
    if len(strategy.performances.timestamps):
        max_performances = max(strategy.performances.market)
        ax4.clear()
        ax5.clear()
        ax4.axhline(strategy.initial_value, alpha=0.2, color="red", linestyle='-', linewidth=0.8)
        if max_performances > MAX_TRADE_LIMIT:
            ax4.axhline(MAX_TRADE_LIMIT, color="green", linestyle='-', linewidth=0.8)
        for i in range(0, (strategy.performances.timestamps[-1] - strategy.performances.timestamps[0]) // (FOUR_WEEKS)):         
            ax4.axvline(strategy.performances.timestamps[0] + i * (FOUR_WEEKS ), alpha=0.2, color='#808080', linestyle='-', linewidth=0.8)
            ax5.axvline(strategy.performances.timestamps[0] + i * (FOUR_WEEKS ), alpha=0.2, color='#808080', linestyle='-', linewidth=0.8)
     
        ax4.plot(strategy.performances.timestamps, strategy.performances.euros, color='black', linewidth = 0.5)    
        ax4.plot(strategy.performances.timestamps, strategy.performances.saved, color='gold', linewidth = 0.5)    
        ax5.set_ylim([0, max_performances])
        ax5.plot(strategy.performances.timestamps, strategy.performances.market, color='black', linewidth = 0.5)

    plt.tight_layout()
    if apply_strategy:
        global FIG_COUNT
        savefig("frames/{:04x}.png".format(FIG_COUNT), transparent=False, dpi='figure', format="png",
          metadata=None, bbox_inches=None, pad_inches=0.1,
          facecolor='auto', edgecolor='auto', backend=None
        )
        FIG_COUNT += 1

stream = StreamMarketData("ADAEUR", 60, 64, 256, 1024)
strategy = Strategy(200)

#https://stackoverflow.com/questions/41025187/matplotlib-add-subplot-odd-number-of-plots

fig = plt.figure(figsize=(6, 4))
gs = gridspec.GridSpec(nrows=6, ncols=2, )
gs.tight_layout(fig)

ax1 = fig.add_subplot(gs[:4, 0])
ax1.xaxis.set_major_locator(mdates.DayLocator())
ax1.xaxis.set_minor_locator(mdates.HourLocator())
    
ax2 = fig.add_subplot(gs[4:, 0])
ax4 = fig.add_subplot(gs[:3, 1])
ax5 = fig.add_subplot(gs[3:, 1])

ani = animation.FuncAnimation(fig, animate, interval=1000)

plt.show()
